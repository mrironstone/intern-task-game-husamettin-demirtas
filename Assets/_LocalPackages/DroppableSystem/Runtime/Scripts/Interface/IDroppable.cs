﻿using System.Collections.Generic;
using UnityEngine.Events;

namespace DroppableSystem
{
    public interface IDroppable
    {
        UnityEvent OnDropped { get; }
        bool IsDropped { get; }
        void GetDropped();
    }
}
