using System.Collections.Generic;
using UnityEngine.Events;

namespace StackSystem
{
    public interface ICanStack
    {
    
        UnityEvent<IStackable> OnStack { get; }
        UnityEvent<IStackable> OnUnStack { get; }
        void Stack(IStackable IStackable);
        void StackList(List<IStackable> IStackables);
        void UnStack(IStackable IStackable);

    }
}