using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace StackSystem
{
    public class StackElement : MonoBehaviour, IStackable
    {
        public Action<StackElement> OnRemoved;

        public UnityEvent OnStacked => throw new NotImplementedException();

        public UnityEvent OnUnStacked => throw new NotImplementedException();

        public bool IsStacked => throw new NotImplementedException();

        public void GetStacked()
        {
            throw new NotImplementedException();
        }

        public void GetUnstacked()
        {
            throw new NotImplementedException();
        }
        // [SerializeField] StackData stackData;
        // public Vector3 TargetPosition;
    }
}