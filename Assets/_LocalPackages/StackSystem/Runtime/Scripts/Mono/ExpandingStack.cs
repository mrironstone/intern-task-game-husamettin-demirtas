using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StackSystem
{
    public class ExpandingStack : RectangularStack
    {
        protected override IEnumerator IECreateStack()
        {
            for (int i = 0, k = 0; i < PositionList.Count; ++i)
            {
                for (int j = 0; j < PositionList[i].Count && k < ElementList.Count; ++j, ++k)
                {
                    yield return new WaitForSeconds(TotalStackDelay);
                    if(UsePrefab.Enabled && i == 0)
                    {
                        StackElement element = Instantiate(UsePrefab.Value, PositionList[i][j], Quaternion.identity).GetComponent<StackElement>();

                        ElementList[k] = element;
                        if(LocalMovement)
                        {
                            element.transform.parent = StackCenterTransform;
                            element.transform.localPosition = PositionList[i][j];
                        }
                        StartStackMovement(element, PositionList[i][j]);
                    }
                    else if(UsePrefab.Enabled)
                    {
                        StackElement element = Instantiate(UsePrefab.Value, PositionList[i-1][j], Quaternion.identity).GetComponent<StackElement>();

                        ElementList[k] = element;
                        if(LocalMovement)
                        {
                            element.transform.parent = StackCenterTransform;
                            element.transform.localPosition = PositionList[i-1][j];
                        }
                        StartStackMovement(element, PositionList[i][j]);
                    }
                    else
                        StartStackMovement(ElementList[k], PositionList[i][j]);
                }
            }
            yield return new WaitForSeconds(TotalStackDelay);
            OnStackCompleted?.Invoke();
        }
    }
}