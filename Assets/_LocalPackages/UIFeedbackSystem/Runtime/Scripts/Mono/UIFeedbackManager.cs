using UnityEngine;
using UnityEngine.UI;

namespace UIFeedbackSystem
{
    public class UIFeedbackManager : MonoBehaviourSingleton<UIFeedbackManager>
    {
        [SerializeField] Image UIFeedbackImage;
        [SerializeField] Animator animator;

        private void Start()
        {
            DisableFeedbackImage();
        }

        public void ShowFeedback(UIFeedbackData data)
        {
            UIFeedbackImage.sprite = data.FeedbackSprite.Value;
            UIFeedbackImage.enabled = true;
            animator.SetTrigger(data.AnimationTrigger.Value);
            Invoke(nameof(DisableFeedbackImage), data.FeedbackTime.Value);
        }

        void DisableFeedbackImage()
        {
            UIFeedbackImage.enabled = false;
        }

    }
}