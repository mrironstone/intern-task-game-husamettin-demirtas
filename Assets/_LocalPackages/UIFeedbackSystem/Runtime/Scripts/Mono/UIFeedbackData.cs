﻿using ScriptableSystem;
using Sirenix.OdinInspector;
using UnityEngine;

namespace UIFeedbackSystem
{
    [CreateAssetMenu(menuName = "UIFeedbackData/UIFeedbackData")]
    public class UIFeedbackData : ScriptableObject
    {
        public SpriteReference FeedbackSprite;
        public FloatReference FeedbackTime;
        public StringReference AnimationTrigger;

        [Button]
        public void ShowFeedback()
        {
            UIFeedbackManager.Instance.ShowFeedback(this);
        }
    }
}