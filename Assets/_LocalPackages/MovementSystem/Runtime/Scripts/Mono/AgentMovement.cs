using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AgentSystem
{
    public class AgentMovement : MonoBehaviour
    {
        Rigidbody rb;
        float moveSpeed = 7.5f;
        // CharacterController characterController;

        Vector3 movementVector = Vector3.zero;
        bool canMove = true;

        void Awake()
        {
            // characterController = GetComponent<CharacterController>();
            rb = GetComponent<Rigidbody>();

        }

        private void OnEnable()
        {
            GetComponent<Agent>().OnAgentMoveSpeedChanged += SetMoveSpeed;
        }

        private void OnDisable()
        {
            GetComponent<Agent>().OnAgentMoveSpeedChanged -= SetMoveSpeed;
        }

        public void MoveCharacter(Vector2 inputVector)
        {
            //Vector3 movementVector = new Vector3(inputVector.x, 0, inputVector.y);
            //characterController.Move(movementVector.normalized  * moveSpeed * Time.deltaTime);

            movementVector = new Vector3(inputVector.x, 0, inputVector.y);
            //movementVector = movementVector.normalized * moveSpeed;

        }

        private void FixedUpdate()
        {
            if (movementVector == Vector3.zero) return;
            if(!canMove) return;

            Vector3 direction = new Vector3(movementVector.x, 0, movementVector.z);
            direction = direction; //this was normalized

            Vector3 targetVelocity = direction;
            targetVelocity *= moveSpeed;
            Vector3 velocity = rb.velocity;

            float maxVelocityChange = moveSpeed * 3;

            Vector3 velocityChange = (targetVelocity - velocity);
            velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
            velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);

            //rb.AddForce(velocityChange, ForceMode.Impulse);
            //rb.MovePosition(transform.position + movementVector * moveSpeed * 0.001f);
            rb.AddForce(velocityChange * Time.deltaTime * 75, ForceMode.Impulse);
        }

        public float GetVelocity()
        {
            //return characterController.velocity.magnitude;
            return rb.velocity.magnitude;
            //return rb.velocity.x;
        }

        public void ToggleMovement(bool value)
        {
            canMove = value;
        }

        void SetMoveSpeed(float speed)
        {
            moveSpeed = speed;
        }
    }

}