using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class MoveBetweenTwoTransform : MonoBehaviour
{
    [SerializeField] Transform startTransform;
    [SerializeField] Transform endTransform;
    [SerializeField] float maxValue=1;
    [SerializeField] float moveStep = 1;
    [ReadOnly] [SerializeField] float currentValue;
    public float CurrentValue { get { return currentValue; } }

    private void Start()
    {
        SetCurrentValue(0);
    }

    [Button("Move To Start")]
    public void MoveToStart()
    {
        SetCurrentValue(currentValue - moveStep * Time.deltaTime*.1f);
    }

    [Button("Move To End")]
    public void MoveToEnd()
    {
        SetCurrentValue(currentValue + moveStep * Time.deltaTime * .1f);
    }

    [Button("Set Current Value")]
    public void SetCurrentValue(float value)
    {
        currentValue = Mathf.Clamp(value,0,maxValue);
        UpdateTransform();
    }

    [Button("Update Transform")]
    void UpdateTransform()
    {
        transform.position = Vector3.Lerp(startTransform.position, endTransform.position, currentValue / maxValue);
        transform.rotation = Quaternion.Lerp(startTransform.rotation, endTransform.rotation, currentValue / maxValue);
    }

    [Button("Set Start Transform")]
    void SetStartTransform()
    {
        startTransform.position = transform.position;
        startTransform.rotation = transform.rotation;
    }

    [Button("Set End Transform")]
    void SetEndTransform()
    {
        endTransform.position = transform.position;
        endTransform.rotation = transform.rotation;
    }
}
