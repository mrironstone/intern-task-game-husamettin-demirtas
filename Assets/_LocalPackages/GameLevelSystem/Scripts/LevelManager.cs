using UnityEngine.SceneManagement;
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ElephantSDK;
using Sirenix.OdinInspector;

namespace GameLevelSystem
{
    public class LevelManager : MonoBehaviourSingleton<LevelManager>
    {
        int _tutorialIndex = -1;
        public int TutorialIndex
        {
            get
            {
                if(_tutorialIndex == -1)
                    _tutorialIndex = PlayerPrefs.GetInt("TutorialIndex", 0);
                return _tutorialIndex;
            }
            set
            {
                _tutorialIndex = value;
                PlayerPrefs.SetInt("TutorialIndex", _tutorialIndex);
            }
        }
        public LevelData LevelData 
        {
            get
            {
                if(TutorialActive)
                    return tutorialLevels[TutorialIndex];
                return levelList[currentLevelIndex];
            }
        }
        public static LevelController CurrentLevelController = null;
        [SerializeField] List<LevelData> tutorialLevels = new List<LevelData>();
        [SerializeField] List<LevelData> levelList;
        int currentLevelIndex = 0;
        int currentLevelGroup = 1;
        public const int GameSceneIndex = 1;
        public const int FirstLevelIndex = 2;
        public int CurrentMapIncrement
        {
            get 
            {
                return (currentLevelGroup - 1) * levelList.Count;
            }
        }
        public int CurrentMapIndex { get { return LevelData.MapIndex + CurrentMapIncrement; } }
        [ReadOnly] public bool TutorialActive;

        void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
            LevelController.OnLevelPassed += OnLevelPassed;
            LevelController.OnLevelFailed += OnLevelFailed;
        }

        void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
            LevelController.OnLevelPassed -= OnLevelPassed;
            LevelController.OnLevelFailed -= OnLevelFailed;
        }

        private void Awake()
        {
            if(PlayerPrefs.HasKey("CurrentLevel"))
                currentLevelIndex = PlayerPrefs.GetInt("CurrentLevel") % levelList.Count;
            if(PlayerPrefs.HasKey("LevelGroup"))
                currentLevelGroup = PlayerPrefs.GetInt("LevelGroup");
            if(tutorialLevels.Count > 0 && !PlayerPrefs.HasKey("TutorialPlayed"))
            {
                TutorialActive = true;
                SceneManager.LoadSceneAsync(tutorialLevels[TutorialIndex].SceneIndex, LoadSceneMode.Additive);
            }
            else
                SceneManager.LoadSceneAsync(levelList[currentLevelIndex].SceneIndex, LoadSceneMode.Additive);
        }

        void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            if(scene.buildIndex >= FirstLevelIndex && TutorialActive)
                Elephant.LevelStarted(0);
            else if(scene.buildIndex >= FirstLevelIndex)
                Elephant.LevelStarted(currentLevelIndex + 1);
        }

        void OnLevelPassed()
        {
            if(TutorialActive)
            {
                Elephant.LevelCompleted(0);
                ++TutorialIndex;
                if(TutorialIndex >= tutorialLevels.Count)
                    PlayerPrefs.SetInt("TutorialPlayed", 1);
            }
            else
            {
                Elephant.LevelCompleted(currentLevelIndex + 1);
                PlayerPrefs.SetInt("CurrentLevel", (currentLevelIndex + 1) % levelList.Count);
            }
        }

        void OnLevelFailed()
        {
            Elephant.LevelFailed(currentLevelIndex + 1);
        }
        [Button("Reload Level")]
        public void ReloadLevel()
        {
            LevelController.OnLevelReloadStarted?.Invoke();
            if(TutorialActive)
            {
                SceneManager.UnloadSceneAsync(tutorialLevels[TutorialIndex].SceneIndex);
                SceneManager.LoadSceneAsync(tutorialLevels[TutorialIndex].SceneIndex, LoadSceneMode.Additive);
            }
            else
            {
                SceneManager.UnloadSceneAsync(levelList[currentLevelIndex].SceneIndex);
                SceneManager.LoadSceneAsync(levelList[currentLevelIndex].SceneIndex, LoadSceneMode.Additive);
            }
        }
        [ContextMenu("Pass Level")]
        public void LoadNextLevel()
        {
            Invoke(nameof(StartLoad), 1.25f);
            // StartLoad();
            // SceneManager.UnloadSceneAsync(levelList[currentLevelIndex].SceneIndex);
            // currentLevelIndex = ++currentLevelIndex % levelList.Count;
            // SceneManager.LoadSceneAsync(levelList[currentLevelIndex].SceneIndex, LoadSceneMode.Additive);
            // GameStateManager.OnGameLevelUnloaded?.Invoke();
        }

        [Button("Next Level")]
        void StartLoad()
        {
            if(TutorialActive)
            {
                SceneManager.UnloadSceneAsync(tutorialLevels[TutorialIndex-1].SceneIndex);
            }
            else
            {
                SceneManager.UnloadSceneAsync(levelList[currentLevelIndex].SceneIndex);
                if(currentLevelIndex == levelList.Count - 1)
                IncrementLevelIndexes();
                currentLevelIndex = ++currentLevelIndex % levelList.Count;
            }
            if(TutorialIndex >= tutorialLevels.Count)
            {
                TutorialActive = false;
                SceneManager.LoadSceneAsync(levelList[currentLevelIndex].SceneIndex, LoadSceneMode.Additive);
            }
            else
            {
                SceneManager.LoadSceneAsync(tutorialLevels[TutorialIndex].SceneIndex, LoadSceneMode.Additive);
            }
        }

        public void IncrementLevelIndexes()
        {
            PlayerPrefs.SetInt("LevelGroup", ++currentLevelGroup);
        }

        public void CallLevelPassed()
        {
            LevelController.OnLevelPassed?.Invoke();
        }

        public void CallLevelFailed()
        {
            LevelController.OnLevelFailed?.Invoke();
        }
    }
}