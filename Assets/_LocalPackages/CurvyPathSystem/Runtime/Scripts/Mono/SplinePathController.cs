using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FluffyUnderware.Curvy.Controllers;
using ScriptableSystem;
using UnityEngine;
[RequireComponent(typeof(SplineController))]
public class SplinePathController : MonoBehaviour
{
    [SerializeField] ScriptableFloat initialSpeed;
    [SerializeField] ScriptableFloat currentSpeed;
    [SerializeField] ScriptableFloat currentDistance;
    [SerializeField] bool stopOnStart = true;
    SplineController splineController;
    void Awake()
    {
        splineController = GetComponent<SplineController>();
        splineController.OnControlPointReached.AddListener(OnControlPointReached);
        currentSpeed.UpdateValue(stopOnStart ? 0 : initialSpeed.GetValue());
        UpdateSpeed();
    }

    void OnEnable()
    {
        currentSpeed.OnValueUpdated += UpdateSpeed;
        currentDistance.OnValueUpdated += UpdateDistance;
    }

    void OnDisable()
    {
        currentSpeed.OnValueUpdated -= UpdateSpeed;
        currentDistance.OnValueUpdated -= UpdateDistance;
    }

    private void Update()
    {
        currentDistance.UpdateValue(splineController.AbsolutePosition,false);
    }

    public void OnControlPointReached(CurvySplineMoveEventArgs curvySplineMoveEventArgs)
    {
        var segmentEvent = curvySplineMoveEventArgs.ControlPoint.GetComponent<SegmentEvent>();
        if (segmentEvent)
            segmentEvent.OnControlPointReached(splineController);
    }
    void UpdateSpeed()
    {
        splineController.Speed = currentSpeed.GetValue();
    }

    void UpdateDistance()
    {
        splineController.AbsolutePosition = currentDistance.GetValue();
    }
}