using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class FeedbackText : MonoBehaviour
{
    [SerializeField] TMP_Text text;
    [SerializeField] RectTransform rect;
    float moveAmount = 0.2f;
    float moveSpeed = 0.5f;

    [SerializeField] Color red;
    [SerializeField] Color green;
    [SerializeField] Color white;
    [SerializeField] Color yellow;

    void Awake()
    {
        rect = GetComponent<RectTransform>(); 
        text = GetComponent<TMP_Text>();
    }

    private void OnEnable()
    {
        rect.DOLocalMoveY(rect.position.y + moveAmount, moveSpeed).SetEase(Ease.InSine);
        text.DOFade(0, moveSpeed).SetEase(Ease.InCubic);
        rect.DOScale(Vector3.one * 0.01f, moveSpeed).SetEase(Ease.InBack);
        Invoke(nameof(SelfDestroy), moveSpeed);
    }

    public void SetDamageAmount(int damage)
    {
        if (damage > 0)
            text.text = "+" + damage.ToString();
        else
            text.text = damage.ToString();
    }

    public void SetColorRed()
    {
        text.color = red;

    }

    public void SetColorGreen()
    {
        text.color = green;
    }

    public void SetColorWhite()
    {
        text.color = white;
    }

    public void SetColorYellow()
    {
        text.color = yellow;
    }

    void SelfDestroy()
    {
        Destroy(gameObject);
    }
}
