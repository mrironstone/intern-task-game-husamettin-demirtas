using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Sirenix.OdinInspector;
public class ShakePositionFeedback : MonoBehaviour
{
    [SerializeField] float duration;
    [SerializeField] float strength = 1;
    [SerializeField] int vibrato = 10;
    [SerializeField] float randomness = 90;

    [Button]
    public void Play()
    {
        transform.DOShakePosition(duration, strength, vibrato, randomness);
    }
}
