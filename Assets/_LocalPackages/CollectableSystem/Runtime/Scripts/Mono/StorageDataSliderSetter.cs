﻿using UnityEngine;
using UnityEngine.UI;

namespace CollectableSystem
{
    public class StorageDataSliderSetter : StorageDataUISetter
    {
        [SerializeField]
        Slider slider;

        public override void OnDataUpdated()
        {
            slider.value = storageData.Ratio * slider.maxValue;
        }
    }
}
