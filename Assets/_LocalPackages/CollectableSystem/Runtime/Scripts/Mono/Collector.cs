using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CollectableSystem
{
    public class Collector : MonoBehaviour
    {
        [SerializeField]
        [Range(1f,30f)]
        float collectSpeed = 5f;
        [SerializeField]
        [Range(1, 20)]
        float collectRadius = 5;
        List<ICollectable> collectList = new List<ICollectable>();

        //virtual protected bool canCollectItem { get { return true; } }

        protected virtual void Awake()
        {
            transform.localScale = new Vector3(collectRadius * 2, collectRadius * 2, collectRadius * 2);
            StartCoroutine(IECollect());
        }

        private void OnTriggerEnter(Collider other)
        {
            ICollectable collectable = other.GetComponent<ICollectable>();
            if (collectable != null && !collectable.IsCollected && CanCollectItem(collectable))
                collectList.Add(collectable);
        }

        protected virtual IEnumerator IECollect()
        {
            List<ICollectable> removeList = new List<ICollectable>();
            while(true)
            {
                for (int i = 0; i < collectList.Count; ++i)
                {
                    var collectable = collectList[i];
                    if (collectable == null) continue;
                    var collectObject = ((Component)collectable).gameObject;
                    if (collectObject == null) continue;
                    float step = collectSpeed * Time.deltaTime;
                    collectObject.transform.position = Vector3.Lerp(collectObject.transform.position, transform.position, step);
                    if (Vector3.Distance(collectObject.transform.position, transform.position) < 0.5f)
                    {
                        CollectItem(collectable);
                        removeList.Add(collectable);
                    }
                   // Debug.Log("Object moved");
                }

                for (int i = 0; i < removeList.Count; ++i)
                {
                    collectList.Remove(removeList[i]);
                }
                removeList.Clear();

                //yield return new WaitForSeconds(collectTickSpeed);
                yield return null;
            }
        }

        protected virtual void CollectItem(ICollectable collectable)
        {
            collectable.GetCollected();
        }

        protected virtual bool CanCollectItem(ICollectable collectable)
        {
            return true;
        }

        void OnDrawGizmosSelected()
        {
#if UNITY_EDITOR
            UnityEditor.Handles.color = Color.red;
            UnityEditor.Handles.DrawWireDisc(transform.position, transform.up, collectRadius);
#endif
        }
    }
}