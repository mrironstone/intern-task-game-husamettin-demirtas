﻿using UnityEngine;

namespace CollectableSystem
{
    public class OnTriggerEnterCollector : OnTriggerCollector
    {
        private void OnTriggerEnter(Collider other)
        {
            OnTrigger(other.gameObject);
        }
    }

}
