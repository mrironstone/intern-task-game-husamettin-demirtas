﻿using UnityEngine;
using TMPro;
namespace CollectableSystem
{
    public class StorageDataTmpTextSetter : StorageDataUISetter
    {
        [SerializeField] TMP_Text currentValueText;
        [SerializeField] TMP_Text maxValueText;

        public override void OnDataUpdated()
        {
            if (currentValueText)
                currentValueText.text = storageData.GetCurrentValue().ToString();
            if (maxValueText)
                maxValueText.text = storageData.CurrentMaxValue.ToString();
        }
    }
}