﻿using UnityEngine;
namespace CollectableSystem
{
    public abstract class StorageDataUISetter : MonoBehaviour
    {
        [SerializeField] protected StorageData storageData;

        private void OnEnable()
        {
            storageData.OnValueChanged += OnDataUpdated;
        }

        private void OnDisable()
        {
            storageData.OnValueChanged -= OnDataUpdated;
        }

        public abstract void OnDataUpdated();
    }
}