﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace CollectableSystem
{
    public class GeneralCollector : MonoBehaviour, ICanCollect
    {
        [ReadOnly] [SerializeField] List<ICollectable> collected = new List<ICollectable>();
        public List<ICollectable> GetCollected => collected;

        [SerializeField] UnityEvent<ICollectable> onCollect;
        public UnityEvent<ICollectable> OnCollect => onCollect;

        public void Collect(ICollectable ICollectable)
        {
            collected.Add(ICollectable);
            OnCollect?.Invoke(ICollectable);
        }
    }

}