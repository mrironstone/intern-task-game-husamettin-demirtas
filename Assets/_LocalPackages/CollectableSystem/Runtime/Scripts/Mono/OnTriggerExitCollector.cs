﻿using UnityEngine;

namespace CollectableSystem
{
    public class OnTriggerExitCollector : OnTriggerCollector
    {
        private void OnTriggerExit(Collider other)
        {
             OnTrigger(other.gameObject);
        }
    }

}
