﻿using UnityEngine;

namespace CollectableSystem
{
    public class StorageDataToggler : StorageDataUISetter
    {
        [SerializeField]
        GameObject toggleObject;
        [SerializeField]
        bool toggleOnAtFull;

        public override void OnDataUpdated()
        {
            if (!toggleObject) return;
            if (toggleOnAtFull)
                toggleObject.SetActive(storageData.IsFull);
            else
                toggleObject.SetActive(!storageData.IsFull);
        }
    }
}