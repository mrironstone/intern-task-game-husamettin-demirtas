﻿using UnityEngine;

namespace CollectableSystem
{
    public class CollectorWithStorage : Collector
    {
        [SerializeField]
        protected StorageData StorageData;
    }
}