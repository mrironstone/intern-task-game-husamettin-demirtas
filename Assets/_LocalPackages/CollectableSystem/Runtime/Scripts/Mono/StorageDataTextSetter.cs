﻿using UnityEngine;
using UnityEngine.UI;
namespace CollectableSystem
{
    public class StorageDataTextSetter : StorageDataUISetter
    {
        [SerializeField] Text currentValueText;
        [SerializeField] Text maxValueText;

        public override void OnDataUpdated()
        {
            if (currentValueText)
                currentValueText.text = storageData.GetCurrentValue().ToString();
            if (maxValueText)
                maxValueText.text = storageData.CurrentMaxValue.ToString();
        }
    }
}