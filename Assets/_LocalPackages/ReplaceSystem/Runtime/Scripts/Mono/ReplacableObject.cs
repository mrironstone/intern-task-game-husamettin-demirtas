﻿using UnityEngine;

namespace ReplaceSystem
{
    [CreateAssetMenu(menuName = "ReplaceSystem/ScriptableData/ReplacableObject")]
    public class ReplacableObject : ScriptableObject
    {
        public GameObject Replacable;
    }

}
