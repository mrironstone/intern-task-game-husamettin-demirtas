namespace DamagableSystem
{
    public interface IDamagable
    {
        public void ApplyDamage(float damage);
    }
}