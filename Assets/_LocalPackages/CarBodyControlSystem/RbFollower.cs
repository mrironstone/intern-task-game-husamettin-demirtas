using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RbFollower : MonoBehaviour
{
    Rigidbody rb, target;

    [SerializeField] float followForce = 0.5f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        if(rb && target)
            rb.AddForce((target.position - rb.position) * followForce);
    }

    public void SetTarget(Rigidbody targetToSet)
    {
        target = targetToSet;
    }
}
