using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarBodyController : MonoBehaviour
{
    [SerializeField] GameObject wholeCar;
    [SerializeField] GameObject body;
    [SerializeField] GameObject WheelFR;
    [SerializeField] GameObject WheelFL;
    Rigidbody rb;
    [SerializeField] RbFollower followerRB;

    float wheelTurnForce = 10; // force to rotate front wheels
    float maxTurnAngle = 23; // front wheels max angle
    float rollForce = 2; //left right body lean force
    float yawForce = 1; //forward-backward body lean force
    float pitchForce = 2; // y axis rotate force
    float maxRollDeg = 8; //left right lean limit
    float MaxYawDeg = 3; //forward backward lean limit
    float maxPitchDeg = 10; //y axis rotate limit

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        followerRB.SetTarget(rb);
        followerRB.transform.parent = null;
    }

    void Update()
    {
        //Vector3 velocityVector = rb.velocity;
        Vector3 velocityVector = transform.position - followerRB.transform.position + Vector3.back;
        // Debug.Log(velocityVector);
        Vector3 rotationVector = new Vector3(Mathf.Clamp(-velocityVector.z * yawForce, -MaxYawDeg, MaxYawDeg), 0, Mathf.Clamp(velocityVector.x * rollForce, -maxRollDeg, maxRollDeg));
        body.transform.localRotation = Quaternion.Euler(rotationVector);
        //clamp values

        Quaternion wheelRot = Quaternion.Euler(Vector3.up * Mathf.Clamp(velocityVector.x * wheelTurnForce, -maxTurnAngle, maxTurnAngle));
        WheelFR.transform.localRotation = wheelRot;
        WheelFL.transform.localRotation = wheelRot;

        wholeCar.transform.rotation = Quaternion.Euler(Vector3.up * Mathf.Clamp(velocityVector.x * pitchForce, -maxPitchDeg, maxPitchDeg));
    }
}
