﻿namespace TriviaSystem
{
    public static partial class Extension
    {
        public static bool IsSame(this TriviaSubject subject, TriviaSubject targetSubject)
        {
            return true;
        }
    }
}
/*
TriviaLevelManager
Subject : TriviaSubject
WordList : TriviaWordData[]
void CheckWords()
bool IsLegit(string word)
bool IsWholeWord(string word)
List<char> GetUniqueLetters()
char[] GetNextChars(string)
List<Letter> GetLetters(List<char>)


LetterCollectableWorldUI
-Texts : Text[]
+SetUI(char)


Letter
onCharacterSet : UnityEvent<char>
Character : char
+MatchAny(string)
void SetCharacter(cha)


CollectableLetter


LetterManager Done
OnWordUpdated : UnityEvent<string>
LastAddedTime : float
currentLevelManager : TriviaLevelManager
Letters : List<Letter>
GetCurrentWord:string {get;}
void TryAdd (Letter Letter)
void TryRemoveLast()
bool IsWordCollected()
+char[] GetNextChars()


LetterSpawner Done
Character : char
bool spawnOnStart;
letterPrefab : Letter 
+void SetCharacter(char)
-void Spawn()
-void Spawn(float delay)


LevelLetterSpawnerCreator
length : int
height : int
levelManager : TriviaLevelManager
spawnerHolder : Transform
void CreateSpawners()

*/