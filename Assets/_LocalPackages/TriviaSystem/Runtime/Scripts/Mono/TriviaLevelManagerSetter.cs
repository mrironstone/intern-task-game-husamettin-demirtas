﻿using UnityEngine;


namespace TriviaSystem
{
    public class TriviaLevelManagerSetter : MonoBehaviour
    {
        [SerializeField] protected TriviaLevelManager triviaLevelManager;

        void OnTriggerEnter(Collider other)
        {
            var letterManager = other.GetComponent<LetterManager>();
            if (letterManager)
                OnLetterManagerEnter(letterManager);
                
        }

        protected virtual void OnLetterManagerEnter(LetterManager letterManager)
        {
            letterManager.SetTriviaManager(triviaLevelManager);

        }
    }

}