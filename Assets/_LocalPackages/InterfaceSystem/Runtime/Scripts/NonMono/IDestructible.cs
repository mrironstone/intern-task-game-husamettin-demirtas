using UnityEngine;
using UnityEngine.Events;

namespace InterfaceSystem
{
    public interface IDestructible
    {
        UnityEvent OnDestructed { get; }
        void Destruct();
    }
}