﻿using System;

namespace GroupMovement.ChainMovementSystem
{
    [Serializable]
    public class ChainControllerData
    {
        public ChainNode PrefabNode;
        public NodeData NodeData;
        public int InitialNodeCount;
    }
}
