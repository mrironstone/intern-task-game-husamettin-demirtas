﻿using System;
using UnityEngine;

namespace GroupMovement.ChainMovementSystem
{
    [Serializable]
    public class PhysicsNodeAlignerWorld : NodeAligner
    {
        Rigidbody rb;
        public PhysicsNodeAlignerWorld(ChainNode node):base(node)
        {
            rb = node.GetComponent<Rigidbody>();
        }

        public override void AlignWithX(Transform target, float alignmentSpeed)
        {
        }

        public override void AlignWithY(Transform target, float alignmentSpeed)
        {
        }

        public override void AlignWithZ(Transform target, float alignmentSpeed)
        {
        }
    }
}
