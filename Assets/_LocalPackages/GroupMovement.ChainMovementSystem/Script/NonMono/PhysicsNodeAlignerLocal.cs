﻿using System;
using UnityEngine;

namespace GroupMovement.ChainMovementSystem
{
    [Serializable]
    public class PhysicsNodeAlignerLocal : NodeAligner
    {
        Rigidbody rb;
        public PhysicsNodeAlignerLocal(ChainNode node) : base(node)
        {
            rb = node.GetComponent<Rigidbody>();
            Debug.Log("PhysicsNodeAlignerLocal created");
        }

        public override void AlignWithX(Transform target, float alignmentSpeed)
        {
            var targetX = target.localPosition.x + NodeData.Offset.x;
            var difference = targetX - nodeTransform.localPosition.x;
            var sign = difference > 0 ? 1 : difference == 0 ? 0 : -1;
            var distance = Mathf.Abs(difference);
            var distanceMultiplier = sign * Mathf.Max(Mathf.Min(distance, NodeData.MaxDistanceMultiplier), NodeData.MinDistanceMultiplier);
            if (distance > NodeData.IgnoreDistance.x)
            {

                var vel = rb.velocity;
                vel.x = alignmentSpeed * Time.deltaTime * distanceMultiplier;
                rb.velocity = vel;
            }
            else
            {
                var newPos = nodeTransform.localPosition;
                newPos.x = targetX;
                nodeTransform.localPosition = newPos;

                var vel = rb.velocity;
                vel.x = 0;
                rb.velocity = vel;
            }
        }

        public override void AlignWithY(Transform target, float alignmentSpeed)
        {
            var targetY = target.localPosition.y + NodeData.Offset.y;
            var difference = targetY - nodeTransform.localPosition.y;
            var sign = difference > 0 ? 1 : difference == 0 ? 0 : -1;
            float distance = Mathf.Abs(difference);
            var distanceMultiplier = sign * Mathf.Max(Mathf.Min(distance, NodeData.MaxDistanceMultiplier), NodeData.MinDistanceMultiplier);
            if (distance > NodeData.IgnoreDistance.y)
            {
                var vel = rb.velocity;
                vel.y = alignmentSpeed * Time.deltaTime * distanceMultiplier;
                rb.velocity = vel;
            }
            else
            {
                var newPos = nodeTransform.localPosition;
                newPos.y = targetY;
                nodeTransform.localPosition = newPos;

                var vel = rb.velocity;
                vel.y = 0;
                rb.velocity = vel;
            }
        }

        public override void AlignWithZ(Transform target, float alignmentSpeed)
        {
            var targetZ= target.localPosition.z+ NodeData.Offset.z;
            var difference = targetZ - nodeTransform.localPosition.z ;
            var sign = difference > 0 ? 1 : difference == 0 ? 0 : -1;
            float distance = Mathf.Abs(difference);
            var distanceMultiplier = sign * Mathf.Max(Mathf.Min(distance, NodeData.MaxDistanceMultiplier), NodeData.MinDistanceMultiplier);
            if (distance > NodeData.IgnoreDistance.z)
            {
                var vel = rb.velocity;
                vel.z = alignmentSpeed * Time.deltaTime * distanceMultiplier;
                rb.velocity = vel;
            }
            else
            {
                var newPos = nodeTransform.localPosition;
                newPos.z = targetZ;
                nodeTransform.localPosition = newPos;

                var vel = rb.velocity;
                vel.z = 0;
                rb.velocity = vel;
            }
        }
    }
}
