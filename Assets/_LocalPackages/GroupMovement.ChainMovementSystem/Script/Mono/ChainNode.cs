using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GroupMovement.ChainMovementSystem
{
    public class ChainNode : Node
    {
        [SerializeField]ChainController chainController;
        public override NodeController NodeController { get => chainController; set => chainController=(ChainController)value; }
        [ReadOnly] [SerializeField] ChainNode parent;
        public ChainNode Parent { get { return parent; } private set { parent = value; } }
        [SerializeField] NodeAligner nodeAligner;
        public bool UseChainControllerNodeData = true;
        [HideIf(nameof(UseChainControllerNodeData))]
        [SerializeField] NodeData nodeData = new NodeData();

        public NodeData NodeData
        {
            get
            {
                if (UseChainControllerNodeData)
                    return chainController ? chainController.ChainControllerData.NodeData : nodeData;
                else
                    return nodeData;
            }
        }


        public bool IsLeader()
        {
            return !chainController.IsNull() && chainController.GetChainLeader() == this;
        }

        protected Rigidbody rb;


        protected virtual void Start()
        {
            rb = GetComponent<Rigidbody>();

            if (NodeData.IsPhysicsMovement)
            {
                Debug.Log("PhysicMovement!");
                if (NodeData.FollowLocal)
                    nodeAligner = new PhysicsNodeAlignerLocal(this);
                else
                    nodeAligner = new PhysicsNodeAlignerWorld(this);
            }
            else
            {
                Debug.Log("Not A PhysicMovement!");
                if (NodeData.FollowLocal)
                    nodeAligner = new TransformNodeAlignerLocal(this);
                else
                    nodeAligner = new TransformNodeAlignerWorld(this);
            }
        }

        public void SetParent(ChainNode parentNode)
        {
            if (parentNode == this)
            {
                Debug.LogError("Trying to set parent itself");
                DestroyNode();
                return;
            }
            Parent = parentNode;
        }
        public List<ChainNode> GetChildNodes()
        {
            return chainController.GetChildNodes(this);
        }

        public List<ChainNode> GetParentNodes()
        {
            return chainController.GetParentNodes(this);
        }

        private void OnValidate()
        {
            if (nodeAligner == null) return;
        }

        public void SetAlignmentSpeed(Vector3 alignmentSpeed)
        {
            NodeData.CurrentAlignmentSpeedX = alignmentSpeed.x;
            NodeData.CurrentAlignmentSpeedY = alignmentSpeed.y;
            NodeData.CurrentAlignmentSpeedZ = alignmentSpeed.z;
        }

        public Vector3 GetAlignmentSpeed()
        {
            return new Vector3(NodeData.CurrentAlignmentSpeedX, NodeData.CurrentAlignmentSpeedY, NodeData.CurrentAlignmentSpeedZ);
        }

        public virtual void FixedUpdate()
        {
            AlignWithParent();
        }

        void AlignWithParent()
        {
            if (!chainController || !Parent) return;
            if (NodeData.AlignX)
            {
                nodeAligner.AlignWithX(Parent.transform, NodeData.CurrentAlignmentSpeedX);
            }

            if (NodeData.AlignY)
            {
                nodeAligner.AlignWithY(Parent.transform, NodeData.CurrentAlignmentSpeedY);

            }

            if (NodeData.AlignZ)
            {
                nodeAligner.AlignWithZ(Parent.transform, NodeData.CurrentAlignmentSpeedZ);
            }
        }

        [Button("Destroy Node")]
        public override void DestroyNode(bool callOnNodeDestroyed = true)
        {
            /*var remove = DestroyNode();
            if (!remove) return;*/

            if (callOnNodeDestroyed)
                OnNodeDestroyed?.Invoke();
           // Destroy(gameObject, .5f);

        }

        public override bool RemoveNode(bool update = true)
        {
            if (!chainController) return false;
            return chainController.RemoveNode(this, false, false, update);
        }

        public void SetNodeData(NodeData data)
        {
            nodeData = data;
        }

        public int GetIndex()
        {
            int index = -1;

            if (chainController)
                index = chainController.GetNodeIndex(this);
            return index;
        }
    }
}

