using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GroupMovement.ChainMovementSystem
{
 
    public class ChainController : NodeController
    {
        public ChainControllerData ChainControllerData;
        [SerializeField] public List<ChainNode> Nodes=new List<ChainNode>();
        public ChainNode LastNode { get { return Nodes.LastOrDefault(); } }
        public override int NodeCount { get{ return Nodes.Count; } }

        public override Node PrefabNode => ChainControllerData.PrefabNode;

        public override int InitialNodeCount => ChainControllerData.InitialNodeCount;

        public override List<Node> GetNodeList => Nodes.Cast<Node>().ToList();

        public Action OnNodeListUpdated;
        public Action<ChainNode> OnLeaderChanged;
        public ChainNode LastLeader;
        ChainNode currentLeader;

        [SerializeField] bool keepLeader = false;



        public override void Initialize()
        {
            CreateInitialNodes();
            SetNodeParents();
            foreach (var node in Nodes)
            {
                node.NodeController = this;
                node.transform.parent = nodeParent;
            }
            if (Nodes.Any())
            {
                Nodes[0].transform.localPosition = Vector3.zero;
            }
        }

        protected override void CreateInitialNodes()
        {
            if (!ChainControllerData.PrefabNode) return;
            CreateNodes(ChainControllerData.PrefabNode,InitialNodeCount);
        }

        public void CreateNodes(ChainNode node,int amount)
        {
            ChainNode[] nodeArray = new ChainNode[amount];
            for (int i = 0; i < amount; i++)
            {
                nodeArray[i] = Instantiate(node, nodeParent);
                if (GetChainLeader())
                {
                    nodeArray[i].transform.position = GetChainLeader().transform.position;
                }
            }
            AddNodes(nodeArray);
        }

        public void SetNodeParents()
        {
            if (Nodes.Count == 0) return;
            Nodes[0].SetParent(null);
            for (int i = 1; i < Nodes.Count; i++)
            {
                Nodes[i].SetParent(Nodes[i - 1]);
            }
        }

        public override void RemoveLastNode(bool update = true)
        {
            RemoveNode(Nodes.Last(), false,false,update);
        }

        public bool RemoveNode(ChainNode node, bool removeParents = false, bool removeChildren = false, bool update = true)
        {
            if (removeParents)
            {
                RemoveParents(node);
            }
            if (removeChildren)
            {
                RemoveChildren(node);
            }
            var value = RemoveNodeFromList(node);
            if (update)
            {            
                OnUpdated();
            }

            return value;
        }

        public override void RemoveNode(Node node, bool update = true)
        {
            RemoveNodeFromList((ChainNode)node);
        }

        public virtual bool RemoveNodeFromList(ChainNode node)
        {
            if (keepLeader && node == GetChainLeader()) return false;
            else if(!keepLeader && node == GetChainLeader())
            {
                Destroy(node.GetComponent<ForwardMovement>());
                Destroy(node.GetComponent<SwerveMovement>());
                node.GetComponent<Rigidbody>().velocity = Vector3.zero;
                Nodes.Remove(node);
                node.transform.parent = null;
                node.NodeController = null;
                node.SetParent(null);
                node.OnNodeDestroyed?.Invoke();
                if (NodeCount <= 0)
                    OnNodeCountBelowZero?.Invoke();
                return true;
            }
            else
            {
                Nodes.Remove(node);
                node.transform.parent = null;
                node.NodeController = null;
                node.SetParent(null);
                node.OnNodeDestroyed?.Invoke();
                return true;
            }
        }

        public void RemoveNodes(int count)
        {
            count = Mathf.Min(count, NodeCount);
            for (int i = 0; i < count; i++)
            {
                RemoveNodeFromList(Nodes.LastOrDefault());
            }
            OnUpdated();
        }

        public void RemoveNodesAtIndex(int index, int amount)
        {
            amount = Mathf.Min(amount, NodeCount);

            for (int i = 0; i < amount; i++)
            {
                index= Mathf.Min(index, NodeCount-1);
                RemoveNodeFromList(Nodes[index]);
            }
            
            OnUpdated();
        }

        public void RemoveNodesFromEnd(int amount)
        {
            RemoveNodesAtIndex(NodeCount - 1, amount);
        }

        void RemoveChildren(ChainNode node)
        {
            if (Nodes.Any())
            {
                var child = Nodes.Last();
                while (child != node)
                {
                    RemoveNodeFromList(child);
                    child = Nodes.Last();
                }
            }
        }

        public List<ChainNode> GetChildNodes(ChainNode node)
        {
            List<ChainNode> childNodes = new List<ChainNode>();

            var child = Nodes.Last();
            while (!child.IsNull() && child != node)
            {
                childNodes.Add(child);
                child = child.Parent;
            }
            return childNodes;
        }

        void RemoveParents(ChainNode node)
        {
            if (Nodes.Any())
            {
                var parent = node.Parent;
                while (parent != null)
                {
                    RemoveNodeFromList(parent);
                    parent = parent.Parent;
                }
            }
        }

        public List<ChainNode> GetParentNodes(ChainNode node)
        {
            List<ChainNode> parentNodes = new List<ChainNode>();

            var parent = node.Parent;
            while (parent != null)
            {
                parentNodes.Add(parent);
                parent = parent.Parent;
            }

            return parentNodes;
        }



        public override void AddNode(Node node, bool update=true)
        {
            Debug.Log("Inside add node");
            var lastTransform = LastNode.transform;
            if(lastTransform == null) Debug.LogError("Node list is empty", gameObject);
            Nodes.Add((ChainNode)node);
            node.transform.parent = nodeParent;
            node.transform.position = lastTransform.position;
            node.transform.rotation = lastTransform.rotation;
            //node.ChainController = this;
            OnNodeAdded((ChainNode)node);
            if(update)
            { 
                OnUpdated();
            }
        }

        public void AddNodeAt(int index, ChainNode node)
        {
            Nodes.Insert(index,node);
            OnNodeAdded(node);
            OnUpdated();
        }

        public void AddNodes(ChainNode[] nodeArray)
        {
            foreach (var node in nodeArray)
            {
                Nodes.Add(node);
                OnNodeAdded(node);
            }
            OnUpdated();
        }


        void OnNodeAdded(ChainNode node)
        {
            node.NodeController = this;
            node.transform.parent = nodeParent;
        }

        [Button("Remove At")]
        public void RemoveNodeAt(int index, bool removeParents = false, bool removeChildren = false)
        {
            if (index < Nodes.Count)
                RemoveNode(Nodes[index], removeParents, removeChildren);
        }

        protected override void OnUpdated()
        {
            Nodes = Nodes.Distinct().ToList();
            SetNodeParents();
            OnNodeListUpdated?.Invoke();
            var leader = GetChainLeader();
            if (leader == null)
                return;
            if (currentLeader!=GetChainLeader())
            {
                // if(currentLeader != null)
                LastLeader = currentLeader;
                currentLeader = GetChainLeader();
                if(currentLeader)
                 OnLeaderChanged?.Invoke(currentLeader);
            }
            for (int i = 0; i < Nodes.Count; i++)
            {
                Nodes[i].name = "Node " + i;
                Nodes[i].transform.parent = nodeParent;
                Nodes[i].NodeController = this;
            }
        }

        public List<ChainNode> GetNodes()
        {
            if(Nodes.Any())
                return Nodes;
            return new List<ChainNode>();
        }

        public void SetChainController(ChainControllerData data)
        {
            ChainControllerData=data;
        }

        public ChainNode GetChainLeader()
        {
            return (ChainNode)GetLeader();
        }

        public int GetNodeIndex(ChainNode node)
        {
            int index = -1;
            for (int i = 0; i < NodeCount; i++)
            {
                if (node == Nodes[i])
                    index = i;
            }

            return index;
        }

        public override void DestroyNode(Node node, bool update = true)
        {
            throw new NotImplementedException();
        }
    }
}
