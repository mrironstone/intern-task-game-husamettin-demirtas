﻿using UnityEngine;
using UnityEngine.Events;

namespace GroupMovement.ChainMovementSystem
{
    public class GroupNodeRemover : MonoBehaviour
    {
        [SerializeField] bool disabeleRenderers = false;
        [SerializeField] protected int numberOfRemove;
        [SerializeField] protected UnityEvent onNodesRemoved;
        

        private void Awake()
        {
            if (disabeleRenderers)
                DisableRenderes();
        }

        private void DisableRenderes()
        {
            var renderers = GetComponentsInChildren<MeshRenderer>();
            foreach (var renderer in renderers)
            {
                renderer.enabled = false;
            }
        }

        public void OnTriggerEnter(Collider other)
        {
            var node = other.GetComponent<ChainNode>();
            var group = node.NodeController?.GetComponentInParent<GroupChainController>();
            if (group == null) return;
            group.RemoveNodes(numberOfRemove,false);
            onNodesRemoved?.Invoke();
        }

    }



}


