using UnityEngine;

namespace AISystem
{
    public class BotPursueState : BotState
    {
        Bot bot;
        Transform chaseTarget;
        Vector3? chaseTargetPosition;
        float randomWaitTime;
        bool path;
        bool isMoveStarted = false;

        public BotPursueState(Bot bot) : base(bot.BotStateMachine, bot.gameObject)
        {
            this.bot = bot;
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();
            //Debug.Log("Inside PlayerPursueState enter");
            var inSightRange = bot.AISensor.IsObjectInRange(bot.SightRange, bot.TargetMask);

            chaseTarget = inSightRange;
            chaseTargetPosition = inSightRange.position;

            if (inSightRange == null || bot.AIMover.IsValidLocation(chaseTargetPosition.Value) == false)
            {
                botStateMachine.SetNewState(bot.GetDefaultState());
            }
        }

        public override void OnStateUpdate()
        {
            if (chaseTarget != null)
            {
                if(!isMoveStarted)
                {
                    bot.AIIntentController.SendMovePosition(transform.position, chaseTargetPosition.Value);
                    isMoveStarted = true;
                }
                if (Vector3.Distance(transform.position, chaseTargetPosition.Value) <= 1f)
                {
                    RemoveTarget();
                    botStateMachine.SetNewState(bot.GetDefaultState());
                }
                else if (Time.time - stateStartTime >= 0.5f)
                {
                    botStateMachine.SetNewState(bot.GetDefaultState());
                }
            }
            else
            {
                RemoveTarget();
                botStateMachine.SetNewState(bot.GetDefaultState());
            }
        }

        public override void OnStateExit()
        {
            isMoveStarted = false;
        }

        void RemoveTarget()
        {
            bot.AIMover.StopMoving();
            chaseTarget = null;
            chaseTargetPosition = null;
        }
    }
}