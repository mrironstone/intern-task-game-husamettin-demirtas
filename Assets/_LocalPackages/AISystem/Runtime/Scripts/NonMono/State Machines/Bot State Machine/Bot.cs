#if UNITY_EDITOR
using UnityEditor;
#endif
using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace AISystem
{
    public class Bot : AI
    {
        [ReadOnly]
        public BotStateMachine BotStateMachine;
        public float SightRange = 18f;
        public float walkPointRange = 10f;
        public LayerMask TargetMask;


        [Button("Initialize AI")]
        public override void Initialize()
        {
            base.Initialize();
            //TargetMask = LayerMask.GetMask("Monster");
            SetTargetLayer();
            inputListener = gameObject.AddComponent<AIInputListener>();
            AgentController.SetInputListener(inputListener);
            inputListener.SetInputs(this);
            AIIntentController.Initialize();
            InitializeStateMachine();
        }

        protected virtual void OnDestroy()
        {
            Destroy(BotStateMachine);
            AgentController?.RemoveListenerActions();
            inputListener?.RemoveInputs(this);
            if(inputListener) Destroy(inputListener);
            RemoveSensors();
        }

        void InitializeStateMachine()
        {
            if (BotStateMachine) Destroy(BotStateMachine);
            BotStateMachine = gameObject.AddComponent<BotStateMachine>();

            var states = CreateStateMachine();

            BotStateMachine.SetStates(states);

            // BotStateMachine.SetStates(AvailableStates);
        }

        protected virtual Dictionary<Type, BotState> CreateStateMachine()
        {
            return new Dictionary<Type, BotState>()
            {
                {typeof(BotIdleState), new BotIdleState(this) },
                {typeof(BotWanderState), new BotWanderState(this) },
                {typeof(BotPursueState), new BotPursueState(this) }
            };
        }

        public override void MoveToPosition(Vector3 position)
        {
            OnMoving?.Invoke(CalculateInputDirection(position));
        }

        public override void ReleaseMove()
        {
            OnMoveRelased?.Invoke();
        }

        public override void AddSensors()
        {
            AISensor = gameObject.AddComponent<Sensor>();
            AIMover = gameObject.AddComponent<AIMover>();
        }

        private Vector2 CalculateInputDirection(Vector3 position) //position to input conversion logic
        {
            Vector3 direction = (position - transform.position).normalized;
            return new Vector2(direction.x, direction.z);
        }

        protected virtual void RemoveSensors()
        {
            Destroy(AISensor);
            Destroy(AIMover);
        }

        public Type GetDefaultState()
        {
            return BotStateMachine.GetDefaultState();
        }

        protected virtual void SetTargetLayer()
        {

        }
        
        #if UNITY_EDITOR
        protected virtual void OnDrawGizmosSelected() 
        {
            Handles.color = Color.blue;
            Handles.DrawWireArc(transform.position, Vector3.up, transform.right, 360, SightRange);
        }
        #endif

    }
}