using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AISystem
{
    public class BotIdleState : BotState
    {
        Bot bot;
        Transform chaseTarget;
        float randomWaitTime;

        public BotIdleState(Bot bot) : base(bot.BotStateMachine, bot.gameObject)
        {
            this.bot = bot;
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();
            //Check sensors


            chaseTarget = null;
            chaseTarget = bot.AISensor.IsObjectInRange(bot.SightRange, bot.TargetMask);

            if (chaseTarget != null)
                botStateMachine.SetNewState(typeof(BotPursueState));
            else
            {
                int backToWanderState = UnityEngine.Random.Range(0, 10);
                if (backToWanderState < 8)
                {
                    botStateMachine.SetNewState(typeof(BotWanderState));
                }
                else
                {
                    randomWaitTime = Random.Range(0, 1f);
                    bot.AIIntentController.SendStopMove();
                    //bot.AIMover.StopMoving();
                }
            }
        }

        public override void OnStateUpdate()
        {
            if (Time.time - stateStartTime > randomWaitTime)
            {
                botStateMachine.SetNewState(bot.GetDefaultState());
            }
        }
    }
}