﻿using UnityEngine;
using UnityEngine.Events;

namespace AddonSystem
{

    [CreateAssetMenu(menuName = "AddonSystem/AddonObject")]
    public class AddonObject : ScriptableObject
    {
        [SerializeField] [Min(1)] public int MaxAddonCount;
        public AddonObject[] RequiredAddons;
        public UnityEvent<GameObject> OnAddonAdded;
        public UnityEvent<GameObject> OnAddonRemoved;
        public bool ApplyDefaultAddedEffectAlways;
        public bool ApplyDefaultRemovedEffectAlways;

        public virtual void DefaultAddonAddedEffect(GameObject go) { OnAddonAdded?.Invoke(go); }
        public virtual void DefaultAddonRemovedEffect(GameObject go) { OnAddonRemoved?.Invoke(go); }

        public void AddAddon(GameObject go)
        {
            var addonController = go.GetComponentInChildren<AddonController>();
            if (addonController)
                AddAddon(addonController);
        }

        public void RemoveAddon(GameObject go)
        {
            var addonController = go.GetComponentInChildren< AddonController>();
            if (addonController)
                RemoveAddon(addonController);
        }

        public void AddAddon(AddonController addonController)
        {
            addonController.TryAddAddon(this);
        }


        public bool HasAddonHasReachedTheCapacity(GameObject go)
        {
            var addonController = go.GetComponentInChildren<AddonController>();
            if (addonController)
                return HasAddonHasReachedTheCapacity(addonController);
            else
                return true;
        }

        public bool HasAddonHasReachedTheCapacity(AddonController addonController)
        {
            return addonController.HasAddonHasReachedTheCapacity(this);
        }

        public void RemoveAddon(AddonController addonController)
        {
            addonController.TryRemoveAddon(this);
        }
    }
}