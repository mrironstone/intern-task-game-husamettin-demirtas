﻿using System;
using ScriptableSystem;
using UnityEngine;

namespace SimilaritySystem
{
    [Serializable]
    public class ColorSimilarityWithRGB : Similarity
    {
        public Color[] CurrentColor;
        public Color[] TargetColor;

        public override float GetSimilarityPercentage()
        {

            float similarity = 0;
            var length = CurrentColor.Length;
            if (length == 0) return 100;
            for (int i = 0; i < length; i++)
            {
                similarity += SimilarityOfColors(CurrentColor[i], TargetColor[i]);
            }
            similarity /= length;
            return similarity;
        }

        public static float SimilarityOfColors(Color color, Color target)
        {
            return (1 - Vector3.Distance(new Vector3(color.r, color.g, color.b), new Vector3(target.r, target.g, target.b)) / 256) * 100;
        }

    }
}
