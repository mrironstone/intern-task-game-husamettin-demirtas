﻿using UnityEngine;

namespace ScriptableSystem.ScriptableCondition
{
    [CreateAssetMenu(menuName = "ScriptableSystem/ScriptableCondition/IntConditionGreater")]
    public class ScriptableIntConditionGreater : ScriptableIntCondition
    {
        public override void SetConditionProvided()
        {
            isConditionProvided = value.GetValue() > comparaValue.Value;
            OnConditionChanged?.Invoke();

        }
    }
}