﻿using UnityEngine;

namespace ScriptableSystem.ScriptableCondition
{
    [CreateAssetMenu(menuName = "ScriptableSystem/ScriptableCondition/FloatConditionGreater")]
    public class ScriptableFloatConditionGreater : ScriptableFloatCondition
    {
        public override void SetConditionProvided()
        {
            isConditionProvided = value.GetValue() > comparaValue.Value;
            OnConditionChanged?.Invoke();

        }
    }
}