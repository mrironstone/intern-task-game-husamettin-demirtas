﻿using UnityEngine;

namespace ScriptableSystem.ScriptableCondition
{
    [CreateAssetMenu(menuName = "ScriptableSystem/ScriptableCondition/IntConditionLess")]
    public class ScriptableIntConditionLess : ScriptableIntCondition
    {
        public override void SetConditionProvided()
        {
            isConditionProvided = value.GetValue() < comparaValue.Value;
            OnConditionChanged?.Invoke();

        }
    }
}