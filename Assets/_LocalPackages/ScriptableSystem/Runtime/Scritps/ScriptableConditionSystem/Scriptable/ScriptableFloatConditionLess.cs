﻿using UnityEngine;

namespace ScriptableSystem.ScriptableCondition
{
    [CreateAssetMenu(menuName = "ScriptableSystem/ScriptableCondition/FloatConditionLess")]
    public class ScriptableFloatConditionLess : ScriptableFloatCondition
    {
        public override void SetConditionProvided()
        {
            isConditionProvided = value.GetValue() < comparaValue.Value;
            OnConditionChanged?.Invoke();

        }
    }
}