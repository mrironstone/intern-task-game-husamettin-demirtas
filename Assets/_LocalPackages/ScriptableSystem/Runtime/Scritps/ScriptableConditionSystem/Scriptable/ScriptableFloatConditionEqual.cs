﻿using UnityEngine;

namespace ScriptableSystem.ScriptableCondition
{
    [CreateAssetMenu(menuName = "ScriptableSystem/ScriptableCondition/FloatConditionEqual")]
    public class ScriptableFloatConditionEqual : ScriptableFloatCondition
    {
        public override void SetConditionProvided()
        {
            isConditionProvided = Mathf.Approximately(value.GetValue(), comparaValue.Value);
            OnConditionChanged?.Invoke();

        }
    }
}