﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace ScriptableSystem.ScriptableCondition
{
    public class MonoConditionChecker : MonoSetter
    {

        [SerializeField] float startListeningDelay=1;
        [SerializeField] bool isListening;
        [SerializeField] bool doListenOnce=true;
        [SerializeField] UnityEvent action;
        [SerializeField] ScriptableConditionList[] scriptableConditionLists;
        [SerializeField] float delay;

        private void Start()
        {
            Invoke(nameof(StartListening), startListeningDelay);
        }

        void StartListening()
        {
            isListening = true;
        }

        public override void Initialize()
        {
            foreach (var scriptableConditionList in scriptableConditionLists)
            {
                scriptableConditionList.OnConditionChanged += OnConditionChanged;
                scriptableConditionList.Initialize();
            }
        }

        public override void DeInitialize()
        {
            foreach (var scriptableConditionList in scriptableConditionLists)
            {
                scriptableConditionList.OnConditionChanged -= OnConditionChanged;
                scriptableConditionList.Deinitialize();
            }
        }

        void OnConditionChanged()
        {
            if (isListening&& IsConditionRealised())
            {
                StartCoroutine(CallAction());
                isListening = false;
                if(!doListenOnce)
                    Invoke(nameof(StartListening), startListeningDelay);
            }
        }

        bool IsConditionRealised()
        {
            if (scriptableConditionLists.Length == 0)
                return false;
            else
            {
                var result = false;
                foreach (var scriptableConditionList in scriptableConditionLists)
                {
                    result |= scriptableConditionList.IsConditionProvided;
                }
                return result;
            }
        }

        IEnumerator CallAction()
        {
            yield return new WaitForSeconds(delay);
                action?.Invoke();
        }

    }

}
