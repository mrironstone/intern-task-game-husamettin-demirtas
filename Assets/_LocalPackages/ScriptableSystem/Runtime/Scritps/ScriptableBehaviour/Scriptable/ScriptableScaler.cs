﻿using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace ScriptableSystem.ScriptableBehaviour
{
    [CreateAssetMenu(menuName = "ScriptableSystem/ScriptableBehaviour/ScriptableScaler")]
    public class ScriptableScaler : ScriptableObject
    {
        [SerializeField] AnimationCurveReference scaleXCurve;
        [SerializeField] AnimationCurveReference scaleYCurve;
        [SerializeField] AnimationCurveReference scaleZCurve;
        [SerializeField] FloatReference scaleTime;
        [SerializeField] UnityEvent<GameObject> onCompleteEvent;


        [Button]
        public void Scale(GameObject go)
        {
            Scale(go, scaleTime.Value, scaleXCurve.Value, scaleYCurve.Value, scaleZCurve.Value);
        }

        [Button]
        public void Scale(GameObject go, float time = 1, AnimationCurve scaleX = null, AnimationCurve scaleY = null, AnimationCurve scaleZ = null)
        {
            MonoCoroutineCallerManager.CallCoroutine(IEScale(go.transform, time, scaleX, scaleY, scaleZ));
        }

        IEnumerator IEScale(Transform tr, float time=1, AnimationCurve scaleX=null, AnimationCurve scaleY = null, AnimationCurve scaleZ = null)
        {
            var deltaTimeCount = time / Time.fixedDeltaTime;
            var stepPerDelta = 1 / deltaTimeCount;
            float currentStep = 0;
            var scale = tr.localScale;
            for (int i = 0; i < deltaTimeCount; i++)
            {
                if (scaleX != null)
                    scale.x = scaleX.Evaluate(currentStep);
                if (scaleY != null)
                    scale.y = scaleY.Evaluate(currentStep);
                if (scaleZ != null)
                    scale.z = scaleZ.Evaluate(currentStep);
                tr.localScale = scale;
                currentStep += stepPerDelta;
                yield return new WaitForSecondsRealtime(Time.fixedDeltaTime);
            }
        }
        
    }
}