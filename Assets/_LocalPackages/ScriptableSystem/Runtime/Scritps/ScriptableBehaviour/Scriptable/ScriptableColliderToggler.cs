﻿using UnityEngine;

namespace ScriptableSystem.ScriptableBehaviour
{
    [CreateAssetMenu(menuName = "ScriptableSystem/ScriptableBehaviour/ScriptableColliderToggler")]
    public class ScriptableColliderToggler : ScriptableToggler<Collider>
    {
        public override void ToggleComponent(Collider component, bool enable)
        {
            component.enabled = enable;
        }
    }
}