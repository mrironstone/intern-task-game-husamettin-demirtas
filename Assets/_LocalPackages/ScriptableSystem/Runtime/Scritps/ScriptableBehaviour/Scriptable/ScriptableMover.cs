﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using System.Collections;

namespace ScriptableSystem.ScriptableBehaviour
{
    [CreateAssetMenu(menuName = "ScriptableSystem/ScriptableBehaviour/ScriptableMover")]
    public class ScriptableMover : ScriptableObject
    {
        [SerializeField] float duration;
        [SerializeField] bool moveDirectly;
        [SerializeField] bool moveRandom;
        [SerializeField] bool limitGlobally;

        [ShowIfGroup("DirectMove", Condition = nameof(moveDirectly))]
        [SerializeField] Vector3 targetOffset;
        [HideIfGroup("AnimationMove", Condition = nameof(moveDirectly))]
        [SerializeField] AnimationCurve moveX;
        [HideIfGroup("AnimationMove")]
        [SerializeField] AnimationCurve moveY;
        [HideIfGroup("AnimationMove")]
        [SerializeField] AnimationCurve moveZ;

        [ShowIfGroup("Random", Condition = nameof(moveRandom))]
        [SerializeField] Vector2 randomX;
        [ShowIfGroup("Random")]
        [SerializeField] Vector2 randomY;
        [ShowIfGroup("Random")]
        [SerializeField] Vector2 randomZ;


        [ShowIfGroup("LimitGlobally", Condition = nameof(limitGlobally))]
        [SerializeField] bool limitXGlobally;
        [ShowIfGroup("LimitGlobally")]
        [ShowIf(nameof(limitXGlobally))]
        [SerializeField] Vector2 limitX;
        [ShowIfGroup("LimitGlobally")]
        [SerializeField] bool limitYGlobally;
        [ShowIfGroup("LimitGlobally")]
        [ShowIf(nameof(limitYGlobally))]
        [SerializeField] Vector2 limitY;
        [ShowIfGroup("LimitGlobally")]
        [SerializeField] bool limitZGlobally;
        [ShowIfGroup("LimitGlobally")]
        [ShowIf(nameof(limitZGlobally))]
        [SerializeField] Vector2 limitZ;

        [SerializeField] UnityEvent<GameObject> onCompleteEvent;

        [Button]
        public void MoveObject(GameObject go)
        {
            if (moveDirectly)
                MoveDirectly(go);
            else
                MoveAnimated(go);
        }

        void MoveDirectly(GameObject go)
        {
            var offset = targetOffset;
            if (moveRandom)
                offset += GetRandomVector();
            Vector3 targetPosition = go.transform.position + offset;
            if (limitGlobally)
                targetPosition = LimitPos(targetPosition);
            go.transform.DOMove(targetPosition, duration).OnComplete(()=>InvokeEvent(go));
        }

        void MoveAnimated(GameObject go)
        {
            MonoCoroutineCallerManager.CallCoroutine(IEMoveAnimated(go));
        }

        IEnumerator IEMoveAnimated(GameObject go)
        {
            float stepMultiplier=1/duration;
            var tr = go.transform;
            var startPos = tr.position;
            Vector3 modifierVector = Vector3.one;
            if(moveRandom){modifierVector = GetRandomVector();}
            Cooldown cooldown = new Cooldown(duration);
            while (!cooldown.IsCooldownFinished)
            {
                SetToAnimatedPos(tr,startPos, modifierVector, stepMultiplier * cooldown.PassedTime);
                yield return null;
            }
            InvokeEvent(go);
        }

        Vector3 GetRandomVector()
        {
            return new Vector3(Random.Range(randomX.x, randomX.y), Random.Range(randomY.x, randomY.y), Random.Range(randomZ.x, randomZ.y));
        }

        void SetToAnimatedPos(Transform tr, Vector3 startPos, Vector3 modifierVector, float step)
        {
            var pos = modifierVector;
            pos.x *= moveX.Evaluate(step);
            pos.y *= moveY.Evaluate(step);
            pos.z *= moveZ.Evaluate(step);
            var newPos= startPos + pos;
            if (limitGlobally)
                newPos = LimitPos(newPos);
            tr.position = newPos;
        }

        Vector3 LimitPos(Vector3 pos)
        {
            if(limitXGlobally)
            {
                pos.x = Mathf.Max(pos.x, limitX.x);
                pos.x = Mathf.Min(pos.x, limitX.y);
            }
            if (limitYGlobally)
            {
                pos.y = Mathf.Max(pos.y, limitY.x);
                pos.y = Mathf.Min(pos.y, limitY.y);
            }
            if (limitZGlobally)
            {
                pos.z = Mathf.Max(pos.z, limitZ.x);
                pos.z = Mathf.Min(pos.z, limitZ.y);
            }
            return pos;
        }

        void InvokeEvent(GameObject go)
        {
            onCompleteEvent?.Invoke(go);
        }

    }
}