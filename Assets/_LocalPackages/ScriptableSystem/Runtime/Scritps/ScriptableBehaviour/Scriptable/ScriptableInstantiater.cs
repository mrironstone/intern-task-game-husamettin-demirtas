﻿using UnityEngine;
namespace ScriptableSystem.ScriptableBehaviour
{
    [CreateAssetMenu(fileName = "Instantiater", menuName = "ScriptableSystem/ScriptableBehaviour/ScriptableInstantiater")]
    public class ScriptableInstantiater : ScriptableObject
    {
        public void Instantiate(GameObject gameObject)
        {
            Object.Instantiate(gameObject);
        }

    }
}