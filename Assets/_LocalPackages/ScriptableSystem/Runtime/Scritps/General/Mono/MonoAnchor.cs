﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace ScriptableSystem
{
    public class MonoAnchor : MonoSetter
    {
        [SerializeField]ScriptableTransfom scriptableAnchor;

        public override void DeInitialize()
        {

        }

        public override void Initialize()
        {
            scriptableAnchor.CurrentTransform = transform;
        }

        [Button("Set Anchor Name")]
        void SetAnchorName()
        {
            name = scriptableAnchor.name;
        }
    }


}
