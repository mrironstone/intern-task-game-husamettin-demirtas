﻿using System.Collections.Generic;
using UnityEngine;


namespace ScriptableSystem
{
    [AddComponentMenu("LocalPackages/ScriptableSystem/KeepOne",0)]
    public class KeepOne : MonoBehaviour
    {
        public ScriptableKeepOneData KeepOneData;
        [ReadOnly]public float TimeStamp;
        private void Awake()
        {
            TimeStamp = Time.realtimeSinceStartup;
            MakeUnique();
        }

        void MakeUnique()
        {
            var allKeepOnes = FindObjectsOfType<KeepOne>(true);
            List<KeepOne> keepOnes = new List<KeepOne>();
            foreach (var keepOne in allKeepOnes)
            {
                if (keepOne.KeepOneData == KeepOneData&&keepOne!=this)
                {
                    keepOnes.Add(keepOne);
                }
            }

            if(keepOnes.Count>0)
            {
                var lowestTimeStamp = TimeStamp;
                for (int i = 0; i < keepOnes.Count; i++)
                {

                    float timeStamp = keepOnes[i].TimeStamp;
                    if (lowestTimeStamp < timeStamp)
                    {
                        Destroy(keepOnes[i].gameObject);
                    }
                    else
                    {
                        lowestTimeStamp = timeStamp;
                    }
                }
                    print("lowestTimeStamp " + lowestTimeStamp );
                if (lowestTimeStamp < TimeStamp)
                    Destroy(gameObject);
            }
                    

            //foreach (var keepOne in allKeepOnes)
            //{
            //    if (keepOne == this) continue;
            //    if(keepOne.KeepOneData==KeepOneData)
            //    {
            //        if (keepOne.IsOriginal) continue;
            //        Destroy(gameObject);
            //        return;
            //    }

            //}
        }
    }


}
