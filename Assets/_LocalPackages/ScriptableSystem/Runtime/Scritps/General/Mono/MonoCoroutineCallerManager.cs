﻿using System.Collections;
using UnityEngine;

namespace ScriptableSystem
{
    public class MonoCoroutineCallerManager : MonoBehaviourSingleton<MonoCoroutineCallerManager>
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        static void OnRuntimeMethodLoad()
        {
            var type = nameof(MonoCoroutineCallerManager);
            if (Instance)
            {
                Debug.Log(type + " is already in scene");
                return;
            }
            DontDestroyOnLoad(new GameObject(type).AddComponent<MonoCoroutineCallerManager>());
            Debug.Log(type + " is added by script");
        }

        public static void CallCoroutine(IEnumerator coroutine)
        {
            Instance.Call(coroutine);
        }

        public void Call(IEnumerator coroutine)
        {
            StartCoroutine(coroutine);
        }
    }
}
