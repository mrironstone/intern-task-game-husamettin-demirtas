﻿using System.Collections;
using UnityEngine;

namespace ScriptableSystem
{
    public class MonoCoroutineCaller : MonoBehaviour
    {
        [SerializeField]
        Object IIEnumeratorObject;
        IIEnumerator IIEnumerator { get { return ((IIEnumerator)IIEnumeratorObject); } }
        [SerializeField] float delay;
        [SerializeField] bool callOnAwake;
        void Awake()
        {
            ResetHasStarted();
            if (callOnAwake)
            Invoke(nameof(CallCoroutine), delay);
        }

        void ResetHasStarted()
        {
            IIEnumerator.HasStarted = false;
        }

        [ContextMenu("Call Coroutine")]
        public void CallCoroutine()
        {
            StartCoroutine(IIEnumerator.GetIEnumerator());
        }

        public void CallCoroutine(IEnumerator coroutine)
        {
            StartCoroutine(coroutine);
        }
    }
}
