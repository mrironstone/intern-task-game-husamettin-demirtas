﻿using UnityEngine;

namespace ScriptableSystem
{

    public class MonoTransformSetter : MonoSetter
    {
        [SerializeField] ScriptableTransformSetter scriptableTransformSetter;

        public override void DeInitialize()
        {
            scriptableTransformSetter.SetTransform -= SetTransform;
        }

        public override void Initialize()
        {
            scriptableTransformSetter.SetTransform += SetTransform;
        }

        private void OnDestroy()
        {
            DeInitialize();
        }

        public void SetTransform(Transform newTransform)
        {
            transform.position = newTransform.position;
            transform.rotation = newTransform.rotation;
        }
    }

}
