﻿using UnityEngine;
using UnityEngine.Events;

namespace ScriptableSystem
{
    [AddComponentMenu("ScriptableSystem/MonoScriptableObjectEventListener")]
    public class MonoScriptableObjectEventListener : MonoSetter
    {
        [SerializeField] ScriptableObjectData data;
        [SerializeField] UnityEvent<ScriptableObject> unityEvent;

        public override void Initialize()
        {
            UpdateValue();
            data.OnValueUpdated += UpdateValue;
        }

        public override void DeInitialize()
        {
            data.OnValueUpdated -= UpdateValue;
        }

        void UpdateValue()
        {
            unityEvent?.Invoke(data.GetValue());
        }
    }
}