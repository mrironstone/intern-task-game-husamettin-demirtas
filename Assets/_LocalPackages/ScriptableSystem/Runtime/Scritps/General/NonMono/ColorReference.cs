﻿using Sirenix.OdinInspector;
using System;
using UnityEngine;

namespace ScriptableSystem
{
    [Serializable]
    [InlineProperty]
    public class ColorReference
    {
        [HorizontalGroup("Color Reference", MaxWidth = 100)]
        [ValueDropdown("valueList")]
        [HideLabel]
        public bool useValue = false;

        [HideIf("useValue", Animate = true)]
        [HorizontalGroup("Color Reference")]
        [HideLabel]
        [Required]
        public ScriptableColor variable;

        [ShowIf("useValue", Animate = true)]
        [HorizontalGroup("Color Reference")]
        [HideLabel]
        public Color constantValue;

        private ValueDropdownList<bool> valueList = new ValueDropdownList<bool>()
        {
            {"Reference",false },
            {"Value", true },
        };

        public Color Value
        {
            get
            {
                return useValue ? constantValue : variable.GetValue();
            }
            set
            {
                if (useValue)
                    constantValue = value;
                else
                    variable.UpdateValue(value);
            }
        }
    }
}
