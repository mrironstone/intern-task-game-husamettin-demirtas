﻿using System;
using UnityEngine.Events;

namespace ScriptableSystem
{
    [Serializable]
    public class ActionWithDelay
    {
        public UnityEvent Action;
        public float DelayTime;
    }
}
