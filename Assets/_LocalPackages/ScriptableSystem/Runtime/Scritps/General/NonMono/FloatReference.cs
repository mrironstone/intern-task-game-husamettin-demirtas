using System;
using Sirenix.OdinInspector;

namespace ScriptableSystem
{

    [Serializable]
    [InlineProperty]
    public class FloatReference
    {
        [HorizontalGroup("Float Reference",MaxWidth = 100)]
        [ValueDropdown("valueList")]
        [HideLabel]
        public bool useValue = true;

        [HideIf("useValue", Animate = true)]
        [HorizontalGroup("Float Reference")]
        [HideLabel]
        [Required]
        public ScriptableFloat variable;

        [ShowIf("useValue", Animate = true)]
        [HorizontalGroup("Float Reference")]
        [HideLabel]
        public float constantValue = 0;

        private ValueDropdownList<bool> valueList = new ValueDropdownList<bool>()
        {
            {"Value", true },
            {"Reference",false },
        };

        public float Value
        {
            get
            {
                return useValue ? constantValue : variable.GetValue();
            }
            set
            {
                if (useValue)
                    constantValue = value;
                else
                    variable.UpdateValue(value);
            }
        }
 
    }
}

