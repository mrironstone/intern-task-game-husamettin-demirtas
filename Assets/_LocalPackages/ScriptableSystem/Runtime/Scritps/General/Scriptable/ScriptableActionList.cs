﻿using System.Collections;
using UnityEngine;

namespace ScriptableSystem
{
    [CreateAssetMenu(fileName = "ActionList", menuName = "ScriptableSystem/ScriptableActionList")]
    public class ScriptableActionList : ScriptableObject, IIEnumerator
    {
        [SerializeField] bool justOnce = true;
        [ReadOnly] [SerializeField] bool hasStarted = false;
        [SerializeField] ActionWithDelay[] actions;

        public bool HasStarted { get { return hasStarted; } set { hasStarted = value; } }



        public IEnumerator GetIEnumerator()
        {
            return CallAcions();
        }

        IEnumerator CallAcions()
        {
            if (!justOnce) hasStarted = false;
            if (!hasStarted)
            {
                hasStarted = true;
                float currentDelay = 0;
                for (int i = 0; i < actions.Length; i++)
                {
                    currentDelay += actions[i].DelayTime;
                    yield return new WaitForSecondsRealtime(currentDelay);
                    actions[i].Action?.Invoke();
                }

            }
        }
    }



}
