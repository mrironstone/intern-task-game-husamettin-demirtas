﻿using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace ScriptableSystem
{
    [CreateAssetMenu(fileName = "IntAction", menuName = "ScriptableSystem/ScriptableAction/ScriptableIntAction")]
    public class ScriptableIntAction : ScriptableObject
    {
        [ReadOnly] public UnityEvent<int> Action;
        public int TestValue;
        [ContextMenu("Call Action")]
        public void CallAction(int value)
        {
            Action?.Invoke(value);
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(ScriptableIntAction))]
    public class ScriptableIntActionEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            ScriptableIntAction action = (ScriptableIntAction)target;
            if (GUILayout.Button("Call Action"))
            {
                action.CallAction(action.TestValue);
            }
        }
    }
#endif

}