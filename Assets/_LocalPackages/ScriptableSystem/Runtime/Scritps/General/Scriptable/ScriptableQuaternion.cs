﻿using Newtonsoft.Json;
using UnityEngine;

namespace ScriptableSystem
{
    [CreateAssetMenu(menuName = "ScriptableSystem/ScriptableData/ScriptableQuaternion")]
    public class ScriptableQuaternion : ScriptableData
    {
        [SerializeField] Quaternion value;

        public override object Value { get { return value; } protected set { this.value = (Quaternion)value; } }
        public Quaternion GetValue() => (Quaternion)Value;

        public override void Load()
        {
            var loadData = JsonConvert.DeserializeObject<Quaternion>(PlayerPrefs.GetString(saveKey, ""));
            UpdateValue(loadData);
        }
    }


}
