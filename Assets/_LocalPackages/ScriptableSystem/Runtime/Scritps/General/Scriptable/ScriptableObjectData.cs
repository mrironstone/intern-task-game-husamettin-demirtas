﻿using Newtonsoft.Json;
using UnityEngine;
namespace ScriptableSystem
{
    [CreateAssetMenu(menuName = "ScriptableSystem/ScriptableData/ScriptableObjectData")]
    public class ScriptableObjectData : ScriptableData
    {
        [SerializeField] ScriptableObject value;

        public override object Value { get { return value; } protected set { this.value = (ScriptableObject)value; } }
        public ScriptableObject GetValue() => (ScriptableObject)Value;

        public override void Load()
        {
            var loadData = JsonConvert.DeserializeObject<ScriptableObject>(PlayerPrefs.GetString(saveKey, ""));
            UpdateValue(loadData);
        }
    }
}
