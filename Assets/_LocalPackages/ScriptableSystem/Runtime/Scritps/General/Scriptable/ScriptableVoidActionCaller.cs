﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace ScriptableSystem
{
    [CreateAssetMenu(fileName = "VoidAction", menuName = "ScriptableSystem/ScriptableAction/ScriptableVoidActionCaller")]
    public class ScriptableVoidActionCaller : ScriptableObject
    {
       [SerializeField] ScriptableVoidAction scriptableVoidAction;
        [Button("Call Action")]
        public void CallAction()
        {
            scriptableVoidAction.CallAction();
        }
    }

////#if UNITY_EDITOR
//    [CustomEditor(typeof(ScriptableVoidAction))]
//    public class ScriptableVoidActionEditor : Editor
//    {
//        public override void OnInspectorGUI()
//        {
//            ScriptableVoidAction action = (ScriptableVoidAction)target;
//            if(GUILayout.Button("Call Action"))
//            {
//                action.CallAction();
//            }
//        }
//    }
////#endif

}