﻿using Newtonsoft.Json;
using UnityEngine;

namespace ScriptableSystem
{


    [CreateAssetMenu(fileName = "Vector3Data", menuName = "ScriptableSystem/ScriptableData/ScriptableVector3")]
    public class ScriptableVector3 : ScriptableData
    {
        [SerializeField] Vector3 value;
        public override object Value { get { return value; } protected set { this.value = (Vector3)value; } }
        public Vector3 GetValue() => (Vector3)Value;

        public override void Load()
        {
            var loadData = JsonConvert.DeserializeObject<Vector3>(PlayerPrefs.GetString(saveKey, ""));
            UpdateValue(loadData);
        }

        public void UpdateX(float x)
        {
            value.x = x;
            OnValueUpdated?.Invoke();
        }

        public void UpdateY(float y)
        {
            value.y = y;
            OnValueUpdated?.Invoke();
        }

        public void UpdateZ(float z)
        {
            value.z = z;
            OnValueUpdated?.Invoke();
        }
    }
}
