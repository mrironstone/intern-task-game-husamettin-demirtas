﻿using UnityEngine;
using UnityEngine.Events;

namespace ScriptableSystem
{
    [CreateAssetMenu(menuName = "ScriptableSystem/ScriptableAnimationCaller")]
    public class ScriptableAnimationCaller : ScriptableObject
    {
        [SerializeField] string trigger;
        [SerializeField] UnityEvent<GameObject> onAnimationCalled;

        public void TriggerAnimation(GameObject go)
        {
            var animator = go.GetComponentInChildren<Animator>();
            animator.SetTrigger(trigger);
        }

        public void TriggerAnimation(GameObject go, string trigger)
        {
            var animator = go.GetComponentInChildren<Animator>();
            if (animator)
            {
                animator.SetTrigger(trigger);
                onAnimationCalled?.Invoke(go);
            }
        }

    }
}