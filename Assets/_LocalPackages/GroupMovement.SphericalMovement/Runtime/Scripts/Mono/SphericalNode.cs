using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GroupMovement.SphericalMovement
{
    public class SphericalNode : Node
    {
        [SerializeField] SphericalController sphericalController;

        public override NodeController NodeController { get => sphericalController; set => sphericalController=(SphericalController)value; }
        [SerializeField] float currentRadius;
        public Vector3 SphericalPosition;

        [SerializeField] UnityEvent<float> onRadiusChanged; 
        [SerializeField] UnityEvent<Vector3> onSphericalPositionChanged;
        [SerializeField] UnityEvent<float> onDistanceToSideChanged;
        public bool IsNodePositioned { get; protected set; }

        public void SetCurrentRadius(float value)
        {
            currentRadius = value;
            onRadiusChanged?.Invoke(currentRadius);
            onDistanceToSideChanged?.Invoke(sphericalController.CurrentSphereSize.Value- currentRadius);
        }

        public float GetCurrentRadius() => currentRadius;
        public void SetSphericalPosition(Vector3 sphericalPosition)
        {
            SphericalPosition = sphericalPosition;
            transform.localPosition = sphericalPosition;
            onSphericalPositionChanged?.Invoke(sphericalPosition);
            IsNodePositioned = true;
        }

        public override bool RemoveNode(bool update = true)
        {
            sphericalController.RemoveNode(this,update);
            return update;
        }

        public override void DestroyNode(bool update = true)
        {
            sphericalController.DestroyNode(this, update);
            //return update;
        }

        [Button]
        public bool CallOnNodeRemoved(bool update = true)
        {
            transform.parent = null;
            OnNodeRemoved?.Invoke();
            IsNodePositioned = false;
            return update;
        }

        [Button]
        public bool CallOnNodeDestroyed(bool update = true)
        {
            transform.parent = null;
            OnNodeDestroyed?.Invoke();
            IsNodePositioned = false;
            return update;
        }


    }
}