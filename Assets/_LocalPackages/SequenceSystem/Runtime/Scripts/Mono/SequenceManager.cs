using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace SequenceSystem
{
    public class SequenceManager : MonoBehaviour
    {
        SequenceData currentSequenceData { get { if (isSequenceAvailable) return sequenceDatas[currentSequenceIndex]; else return null; } }
        [SerializeField] int startSequenceIndex;
        [ReadOnly] [SerializeField] int currentSequenceIndex;
        bool isSequenceAvailable { get { return currentSequenceIndex < sequenceDatas.Length; } }
        [SerializeField] bool doStartSequenceOnStart;
        [SerializeField] float startDelay;
        [ReadOnly] [SerializeField] bool isSequenceOn;
        [SerializeField] UnityEvent onSequenceEnded;
        [SerializeField] SequenceData[] sequenceDatas;
        Coroutine coroutine;
        private void Start()
        {
            if (doStartSequenceOnStart)
                StartSequence();
        }

        private void FixedUpdate()
        {
            if (!isSequenceOn) return;
            OnCurrentSquenceUpdate();
        }

        [Button("Start Sequence")]
        public void StartSequence() { coroutine=StartCoroutine(IEStartSequence()); }
        public void StopSequence() 
        {
            isSequenceOn = false;
            StopCoroutine(coroutine); 
        }
        IEnumerator IEStartSequence()
        {
            yield return new WaitForSecondsRealtime(startDelay);
            isSequenceOn = true;
            foreach (var data in sequenceDatas)
            {
                data.SequenceTrigger.CurrentManager = this;
            }
            SetCurrentStep(startSequenceIndex);
        }

        public void SetCurrentStep(int index)
        {
            if (isSequenceAvailable)
            {
                EndCurrentStep();
            }
            currentSequenceIndex = index;
            OnCurrentSquenceDataStarted();
        }

        void OnCurrentSquenceDataStarted()
        {
            currentSequenceData.OnStart();
            currentSequenceData.SequenceTrigger.OnTriggerCalled += OnCurrentSquenceDataEnded;
        }

        void OnCurrentSquenceUpdate()
        {
            currentSequenceData.OnUpdate();
        }

        void OnCurrentSquenceDataEnded()
        {
            EndCurrentStep();
            MoveToNextStep();
        }

        private void EndCurrentStep()
        {
            if (currentSequenceData == null) return;
            currentSequenceData.OnEnd();
                currentSequenceData.SequenceTrigger.OnTriggerCalled -= OnCurrentSquenceDataEnded;
        }

        private void MoveToNextStep()
        {
            currentSequenceIndex++;
            if (!isSequenceAvailable)
                EndSequence();
            else
                OnCurrentSquenceDataStarted();
        }

        void EndSequence()
        {
            isSequenceOn = false;
            onSequenceEnded?.Invoke();
        }
        public void ResetSequence()
        {
            isSequenceOn = true;
            SetCurrentStep(startSequenceIndex);
        }
    }

}