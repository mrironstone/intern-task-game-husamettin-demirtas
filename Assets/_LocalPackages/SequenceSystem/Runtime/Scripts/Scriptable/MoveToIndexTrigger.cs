using UnityEngine;

namespace SequenceSystem
{
    [CreateAssetMenu(fileName = "MoveToIndexTrigger", menuName = "SequenceSystems/MoveToIndexTrigger")]
    public class MoveToIndexTrigger : SequenceTrigger
    {
        [SerializeField] int index;

        public void MoveToNextIndex()
        {
            OnTriggerCalled?.Invoke();
        }

        public void MoveToIndex(int index)
        {
            CurrentManager.SetCurrentStep(index);
        }

        public override void CallTriggerAction()
        {
            CurrentManager.SetCurrentStep(index);
        }
    }
}