﻿using UnityEngine;

namespace SequenceSystem
{
    [CreateAssetMenu(fileName = "MoveToNextTrigger", menuName = "SequenceSystems/MoveToNextTrigger")]
    public class MoveToNextTrigger : SequenceTrigger
    {

    }
}