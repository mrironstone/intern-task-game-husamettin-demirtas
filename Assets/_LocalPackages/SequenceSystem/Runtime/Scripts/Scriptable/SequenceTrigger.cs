﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace SequenceSystem
{
    public abstract class SequenceTrigger : ScriptableObject
    {
        public Action OnTriggerCalled;
        public SequenceManager CurrentManager { get; set; }

        [Button("Call Trigger Action")]
        public virtual void CallTriggerAction()
        {
            OnTriggerCalled?.Invoke();
        }

        public virtual void OnStart() { }
        public virtual void OnUpdate() { }
        public virtual void OnEnd() { }
    }

}