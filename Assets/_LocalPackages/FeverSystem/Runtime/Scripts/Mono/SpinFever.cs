#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using InterfaceSystem;
using ProgressSystem;
using UnityEngine.Events;

namespace FeverSystem
{
    public class SpinFever : Fever
    {
        [SerializeField] float attackRange;
        [SerializeField] GameObject spinTarget;
        [SerializeField] GameObject attackRadiusSphere;
        [SerializeField] float spinRadius = 1;
        [SerializeField] GameObject projectilePrefab;
        [SerializeField] float rotateSpeed = 10f;
        [SerializeField] int projectileCount = 8;
        List<GameObject> projectileList = new List<GameObject>();
        [SerializeField] Vector3 spinVector = Vector3.up;
        bool isFeverActivatedBefore = false;
        

        protected override void StartFever()
        {
            if(FeverActive) return;
            if(isFeverActivatedBefore) return;
            isFeverActivatedBefore = true;
            spinTarget.SetActive(true);
            CreateProjectiles();
            attackRadiusSphere.transform.localScale = new Vector3(attackRange * 2, attackRange * 2, attackRange * 2);
            base.StartFever();
        }

        public void StopFeverManual()
        {
            StopFever();
        }

        protected override void OnFeverStarted()
        {
            base.OnFeverStarted();
            ToggleChildren(true);

        }

        protected override void OnUpdate()
        {
            Spin();
            if(projectileList.Count == 0)
                StopFever();
        }

        protected override void OnFeverStopped()
        {
            base.OnFeverStopped();
            ToggleChildren(false);
        }

        private void ToggleChildren(bool value)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.SetActive(value);
            }
        }

        protected virtual void Spin()
        {
            spinTarget.transform.Rotate(spinVector * rotateSpeed * Time.deltaTime);
        }

        [Button("Attack")]
        public virtual void Attack(GameObject target)
        {
            var destructible = target.GetComponent<IDestructible>();
            if (destructible != null)
                destructible.Destruct();
        }

        protected virtual void CreateProjectiles()
        {
            for (int i = projectileList.Count; i < projectileCount; ++i)
            {
                var projectile = Instantiate(projectilePrefab, spinTarget.transform);
                projectile.transform.localPosition = Vector3.zero;
                projectileList.Add(projectile);
            }
            AlignProjectiles();
        }

        protected virtual void AlignProjectiles()
        {
            if(projectileList.Count == 0) return;

            spinTarget.transform.eulerAngles = Vector3.zero;
            spinTarget.transform.localPosition = Vector3.zero;
            foreach(var p in projectileList)
            {
                p.transform.eulerAngles = Vector3.zero;
                p.transform.localPosition = Vector3.zero;
            }
            float targetAngle = 360 / projectileList.Count;
            // Debug.Log("Angle: " + Quaternion.AngleAxis(targetAngle, spinTarget.transform.position));
            for (int i = 0; i < projectileList.Count; ++i)
            {
                var rot = Quaternion.AngleAxis(targetAngle * (i + 1),spinVector);
                var lDirection = rot * Vector3.right;
                projectileList[i].transform.localPosition = lDirection * spinRadius;
            }
        }

        GameObject PopProjectile()
        {
            if(projectileList.Count > 0)
            {
                var projectile = projectileList[0];
                projectileList.Remove(projectile);
                AlignProjectiles();
                projectile.transform.parent = null;
                return projectile;
            }
            return null;
        }

        #if UNITY_EDITOR
        private void OnDrawGizmosSelected() 
        {
            Handles.color = Color.red;
            Handles.DrawWireArc(spinTarget.transform.position, spinVector, spinTarget.transform.right, 360, spinRadius);
            Handles.color = Color.blue;
            Handles.DrawWireArc(attackRadiusSphere.transform.position, Vector3.up, spinTarget.transform.right, 360, attackRange);
        }
        #endif
    }   
}