using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace FeverSystem
{
    public class Fever : MonoBehaviour
    {
        [SerializeField] [ReadOnly] protected bool FeverActive;
        [SerializeField] protected float FeverDuration;
        protected float FeverStartTime;
        [SerializeField] UnityEvent onFeverStarted;
        [SerializeField] UnityEvent onFeverStopped;
        
        Coroutine coroutine;
        [Button("Start Fever")]
        protected virtual void StartFever()
        {
            if(coroutine != null)
                StopCoroutine(coroutine);
            OnFeverStarted();
            coroutine = StartCoroutine(IEStartFever());
        }

        [Button("Stop Fever")]
        protected virtual void StopFever()
        {
            if(coroutine != null)
                StopCoroutine(coroutine);
            OnFeverStopped();
        }

        protected virtual IEnumerator IEStartFever()
        {
            while(Time.time - FeverStartTime < FeverDuration)
            {
                OnUpdate();
                yield return null;
            }
            StopFever();
        }

        protected virtual void OnFeverStarted()
        {
            FeverStartTime = Time.time;
            FeverActive = true;
            onFeverStarted?.Invoke();
        }
        protected virtual void OnUpdate()
        {

        }
        protected virtual void OnFeverStopped()
        {
            FeverActive = false;
            onFeverStopped?.Invoke();
        }
    }
}