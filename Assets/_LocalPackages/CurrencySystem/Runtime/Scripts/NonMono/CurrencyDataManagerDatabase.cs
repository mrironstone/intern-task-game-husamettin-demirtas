﻿using System;
using UnityEngine;
using System.Threading.Tasks;

namespace CurrencySystem
{
    [Serializable]
    public class CurrencyDataManagerDatabase : CurrencyDataManager
    {
        int ownerID;
        string key;

        public CurrencyDataManagerDatabase(CurrencyType currencyType, int ownerID=0) : base(currencyType)
        {
            this.ownerID = ownerID;
            key = ownerID + "-" + currencyType.CurrencyName;
        }

        public override void SaveCurrency(CurrencyData data)
        {
            PlayerPrefs.SetString(key, JsonUtility.ToJson(currencyType.Amount));
        }

        public override Task LoadCurrencyDataAsync()
        {
            Debug.Log("Started To Load Currency Data");
            Task.Delay(1000);
            Debug.Log("Ended To Load Currency Data");
            //Task.Run(() => { //return Burada databaseden bilgi cek;
            //});
            var data = JsonUtility.FromJson<float>(PlayerPrefs.GetString(key));
         //   Debug.Log(data.CurrencyType);
            currencyType.Amount = data;
            return Task.CompletedTask;
        }

        public override bool HasDefaultData()
        {
            return PlayerPrefs.HasKey(key);
        }
    }
}