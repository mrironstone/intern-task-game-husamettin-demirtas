﻿using System;

namespace CurrencySystem
{
    [Serializable]
    public class CurrencyConditionData
    {
        public float Value;
        public CurrencyCondition Condition;
    }
}