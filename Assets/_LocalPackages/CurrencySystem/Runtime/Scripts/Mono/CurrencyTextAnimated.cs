using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CurrencySystem;
using TMPro;
using DG.Tweening;
using UnityEngine.Events;

public class CurrencyTextAnimated : MonoBehaviour
{
    [SerializeField] CurrencyType currency;
    TMP_Text currencyText;
    int targetValue;
    int currentValue;
    [SerializeField] float animationDuration = 0.5f;

    [SerializeField] UnityEvent OnCurrencyTextUpdate;

    void Start()
    {
        currencyText = GetComponent<TMP_Text>();
        DOTween.Init();
    }

    private void OnEnable()
    {
        currency.OnCurrencyUpdated += UpdateCurrency;
    }

    private void OnDisable()
    {
        currency.OnCurrencyUpdated -= UpdateCurrency;
    }

    void UpdateCurrency()
    {
        targetValue = (int)currency.Amount;
        DOTween.To(() => currentValue, x => currentValue = x, targetValue, animationDuration).OnUpdate(UpdateUI);
    }

    private void UpdateUI()
    {
        currencyText.text = currentValue.ToString();
        OnCurrencyTextUpdate?.Invoke();
    }
}
