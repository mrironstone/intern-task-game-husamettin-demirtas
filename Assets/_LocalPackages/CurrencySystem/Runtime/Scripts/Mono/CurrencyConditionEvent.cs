﻿using UnityEngine;
using UnityEngine.Events;

namespace CurrencySystem
{
    public class CurrencyConditionEvent : MonoBehaviour
    {
        [SerializeField] CurrencyType currencyType;
        [SerializeField] CurrencyConditionData[] currencyConditions;
        [SerializeField] UnityEvent onConditionRealised;
        [SerializeField] UnityEvent onConditionNotRealised;


        private void OnEnable()
        {
            currencyType.OnCurrencyUpdated += OnCurrencyUpdated;
            OnCurrencyUpdated();
        }

        private void OnDisable()
        {
            currencyType.OnCurrencyUpdated -= OnCurrencyUpdated;
        }

        void OnCurrencyUpdated()
        {
            for (int i = 0; i < currencyConditions.Length; i++)
            {
                if (CheckCondition(currencyConditions[i]))
                {
                    onConditionRealised?.Invoke();
                    return;
                }
            }
            onConditionNotRealised?.Invoke();
        }

        bool CheckCondition(CurrencyConditionData currencyConditionData)
        {
            if (currencyConditionData.Condition == CurrencyCondition.Equal)
            {
                return Mathf.Approximately(currencyType.Amount, currencyConditionData.Value);
            }
            else if (currencyConditionData.Condition == CurrencyCondition.Greater)
            {
                return currencyType.Amount > currencyConditionData.Value;
            }
            else
            {
                return currencyType.Amount < currencyConditionData.Value;
            }
        }
    }
}