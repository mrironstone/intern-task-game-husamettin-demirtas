using System;
using UnityEngine;
using UnityEngine.Events;

namespace CurrencySystem
{

    public abstract class CurrencyText : MonoBehaviour
    {
        [SerializeField] CurrencyType currencyType;
        protected Currency currency;


        private void Start()
        {
            currency = new Currency(currencyType);
            AddTextToCurrency();
            currencyType.OnCurrencyUpdated();
        }

        public abstract void AddTextToCurrency();
    }
}