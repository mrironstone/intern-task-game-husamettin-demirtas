using TMPro;
using UnityEngine;

namespace CurrencySystem
{
    [RequireComponent(typeof(TMP_Text))]
    public class CurrencyTmpText : CurrencyText
    {
        public override void AddTextToCurrency()
        {
            currency.AddText(GetComponent<TMP_Text>());
        }
    }

}
