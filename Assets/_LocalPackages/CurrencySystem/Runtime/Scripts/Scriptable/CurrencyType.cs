﻿using System;
using UnityEngine;

namespace CurrencySystem
{
    [CreateAssetMenu(fileName = "CurrencyType", menuName = "CurrencyType/Type ")]
    public class CurrencyType : ScriptableObject
    {
        public string CurrencyName { get { return name; }}
        // public CurrencyData Data;
        public float Amount = 0;
        public Action OnCurrencyUpdated;

        private void Awake()
        {
            LoadCurrency();
        }

        private void OnEnable()
        {
            OnCurrencyUpdated += SaveCurrency;
        }

        private void OnDisable()
        {
            OnCurrencyUpdated -= SaveCurrency;            
        }

        void SaveCurrency()
        {
            PlayerPrefs.SetFloat(CurrencyName, Amount);
        }

        public void LoadCurrency()
        {
            Amount = PlayerPrefs.GetFloat(CurrencyName, 0);
        }

        [ContextMenu("CallOnCurrencyUpdated")]
        void CallOnCurrencyUpdated()
        {
            OnCurrencyUpdated?.Invoke();
        }
    }
}