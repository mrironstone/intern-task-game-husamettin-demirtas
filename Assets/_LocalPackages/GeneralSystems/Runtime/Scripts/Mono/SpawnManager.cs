using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using FluffyUnderware.Curvy.Controllers;
using GameLevelSystem;
using Sirenix.OdinInspector;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public static Action<GameObject> OnPlayerSpawned;
    [OnValueChanged("TogglePathCallback")]
    public bool SpawnOnPath;
    [OnValueChanged("ToggleLevelControllerCallback")]
    public bool SpawnOnLevelController = true;

    [SerializeField] GameObject playerPrefab;
    GameObject player;
    private void OnEnable() {
        GameStateManager.OnGameLevelLoaded += SpawnPlayer;
    }
    private void OnDisable() {
        GameStateManager.OnGameLevelLoaded -= SpawnPlayer;
    }

    void TogglePathCallback()
    {
        if(SpawnOnPath)
            SpawnOnLevelController = false;
        else
            SpawnOnPath = true;
    }
    void ToggleLevelControllerCallback()
    {
        if(SpawnOnLevelController)
            SpawnOnPath = false;
        else
            SpawnOnLevelController = true;
    }

    void SpawnPlayer()
    {
        if(player) Destroy(player);

        player = Instantiate(playerPrefab, LevelManager.CurrentLevelController.transform);
        if(SpawnOnLevelController)
        {
            player.transform.parent = null;
            player.transform.position = LevelManager.CurrentLevelController.transform.position;
        }
        else if(SpawnOnPath)
        {
            player.transform.parent = FindObjectOfType<SplineController>().transform;
            player.transform.localPosition = Vector3.zero;
            player.transform.localRotation = Quaternion.Euler(Vector3.zero);
        }
        FindObjectOfType<CinemachineTargetGroup>().AddMember(player.transform, 1, 1);
        OnPlayerSpawned?.Invoke(player);
    }
}
