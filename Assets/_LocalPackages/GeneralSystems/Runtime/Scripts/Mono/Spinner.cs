﻿using UnityEngine;

    public class Spinner : MonoBehaviour
    {
        [SerializeField] public bool IsEnabled;
        [SerializeField] bool isSpinning;
        bool isSpinningGetter { get { return IsEnabled && isSpinning && spinSpeed != 0; } }
        [SerializeField] float spinSpeed;
        [SerializeField] bool spinX;
        [SerializeField] bool spinY;
        [SerializeField] bool spinZ;

    public void Update()
    {
        if (Input.GetMouseButtonDown(0) || GameStateManager.Instance.BonusActive)
        {
            SetSpinning(true);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            SetSpinning(false);
        }
    }

    public void FixedUpdate()
        {
            if (isSpinningGetter)
            {
                Spin();
            }
        }

        void Spin()
        {
            if (spinX)
            {
                transform.Rotate(spinSpeed * Time.deltaTime, 0, 0);
            }

            if (spinY)
            {
                transform.Rotate(0, spinSpeed * Time.deltaTime, 0);
            }

            if (spinZ)
            {
                transform.Rotate(0, 0, spinSpeed * Time.deltaTime);
            }
        }

        public void SetSpinning(bool value)
        {
            isSpinning = value;
        }

        public void ToggleSpinning()
        {
            isSpinning = !isSpinning;
        }

        public void SetSpeed(float speedvalue)
        {
             spinSpeed = speedvalue;
        }

        public void SetSpinForXYZ(bool spinForX, bool spinForY, bool spinForZ)
        {
            spinX  = spinForX;
            spinY  = spinForY;
            spinZ  = spinForZ;
        }


    }

