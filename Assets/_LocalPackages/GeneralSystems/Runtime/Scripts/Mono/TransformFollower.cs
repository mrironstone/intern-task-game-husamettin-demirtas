using UnityEngine;
public class TransformFollower : MonoBehaviour
{
    public bool FollowX;
    public bool FollowY;
    public bool FollowZ;
    [SerializeField] protected Transform FollowTarget;
    [SerializeField] float speed = 5;

    protected virtual void Update() 
    {
        if(FollowTarget == null) return;

        transform.position = Vector3.MoveTowards(transform.position, 
            new Vector3(FollowX ? FollowTarget.position.x : transform.position.x, 
            FollowY ? FollowTarget.position.y : transform.position.y,
            FollowZ ? FollowTarget.position.z : transform.position.z), 
            speed * Time.deltaTime);
    }

    public void SetFollowTarget(Transform target)
    {
        FollowTarget = target;
    }
}