using ScriptableSystem;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableImageDuplicateAndMoveFeedback", menuName = "ScriptableManager/ScriptableImageDuplicateAndMoveFeedback")]
public class ScriptableImageDuplicateAndMoveFeedback : ScriptableManager<ImageDuplicateAndMoveFeedback>
{
       public void CreateImages(int amount)
       {
           Instance.CreateImages(amount);
       }
}
