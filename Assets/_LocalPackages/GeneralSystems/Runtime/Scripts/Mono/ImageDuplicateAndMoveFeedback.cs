using System.Collections;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

public class ImageDuplicateAndMoveFeedback : MonoBehaviour
{
    [SerializeField] GameObject prefabImage;
    [SerializeField] Transform start;
    [SerializeField] float randomMinHorizantal=1;
    [SerializeField] float randomMaxHorizantal = 10;
    [SerializeField] float randomMinVertical = 1;
    [SerializeField] float randomMaxVertical = 10;
    [SerializeField] float moveToRandomPosTime=1;
    [SerializeField] Transform target;
    [SerializeField] float toTargetTime=1;
    [Range(0,1)] [SerializeField] float createInterval=0;


    private void Awake()
    {
        prefabImage.SetActive(false);
    }


    [Button("Create Images")]
    public void CreateImages(int amount)
    {
        if(createInterval==0)
        {
            for (int i = 0; i < amount; i++)
            {
                CreateImage();
            }
        }
        else
        {
            StartCoroutine(IECreateImages(amount));
        }


    }

    IEnumerator IECreateImages(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            CreateImage();
            yield return new WaitForSecondsRealtime(createInterval);
        }
    }

    private void CreateImage()
    {
        var image = Instantiate(prefabImage, start.position, start.rotation, start).transform;
        var randomPos = start.position + new Vector3((Random.value < .5 ? 1 : -1) * Random.Range(randomMinHorizantal, randomMaxHorizantal), (Random.value < .5 ? 1 : -1) * Random.Range(randomMinVertical, randomMaxVertical), 0);
        image.gameObject.SetActive(true);
        image.DOMove(randomPos, moveToRandomPosTime).OnComplete(() => MoveToTarget(image));
        StartCoroutine(IEDestroyImage(image.gameObject, 3f));
    }

    void MoveToTarget(Transform transform)
    {
        transform.DOMove(target.position, toTargetTime).OnComplete(() => {
            if(transform)
                Destroy(transform.gameObject);
        } );
    }

    IEnumerator IEDestroyImage(GameObject go, float duration)
    {
        yield return new WaitForSecondsRealtime(duration);
        if(go)
            Destroy(go);
    }
}