using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectToggler : MonoBehaviour
{
    [SerializeField] GameObject[] gameObjects;
    [SerializeField] bool toggleOnStart;
    [SerializeField] int startIndex;


    private void Start()
    {
        if (toggleOnStart)
            Toggle(startIndex);
    }

    public void Toggle(int index)
    {
        for (int i = 0; i < gameObjects.Length; i++)
        {
            gameObjects[i].SetActive(i == index);
        }
    }
}
