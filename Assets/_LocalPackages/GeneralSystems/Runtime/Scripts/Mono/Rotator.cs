using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public RotateData Data;


    // Update is called once per frame
    void Update()
    {
        if(Data.DoRotate)
            transform.Rotate(Data.RotateDirection * Data.RotateSpeed * Time.deltaTime);
    }
}

[Serializable, Toggle("DoRotate")]
public class RotateData
{
    public bool DoRotate = true;
    public float RotateSpeed = 30;
    public Vector3 RotateDirection = Vector3.up;
}