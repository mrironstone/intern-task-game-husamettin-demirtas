using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChildMaker : MonoBehaviour
{
    [SerializeField] Transform parentTransform;

    void OnCollisionEnter(Collision collision)
    {
        if(parentTransform!=null)
            collision.transform.SetParent(parentTransform);
        else
            collision.transform.SetParent(null);
        Debug.Log(parentTransform, parentTransform);
    }
}
