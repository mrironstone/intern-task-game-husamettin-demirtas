using System;
using UnityEngine;

[Serializable]
public class Vector3Toggleable
{
    public bool Enabled;
    public Vector3 Value;
}
