using System;
using ScriptableSystem;

[Serializable]
public class ScriptableFloatToggleable
{
    public bool Enabled;
    public ScriptableFloat Value;
}
