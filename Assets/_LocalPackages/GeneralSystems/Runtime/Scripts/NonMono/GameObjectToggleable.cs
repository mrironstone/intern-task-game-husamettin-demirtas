using System;
using UnityEngine;

[Serializable]
public class GameObjectToggleable
{
    public bool Enabled;
    public GameObject Value;
}
