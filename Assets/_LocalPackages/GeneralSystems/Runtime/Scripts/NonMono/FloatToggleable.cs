using System;

[Serializable]
public class FloatToggleable
{
    public bool Enabled;
    public float Value;
}
