using System;

[Serializable]
public class IntegerToggleable
{
    public bool Enabled = true;
    public int Value;
}
