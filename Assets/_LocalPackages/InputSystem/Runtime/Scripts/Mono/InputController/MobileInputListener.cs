﻿using UnityEngine;

public class MobileInputListener : InputListener
{
    private void OnEnable()
    {
        UIInputManager.OnMoving += CallOnMove;
        UIInputManager.OnMoveJoystickReleased += CallOnMoveReleased;
        //UIInputManager.OnJumpButtonPressed += CallOnJump;
        //UIInputManager.OnWeaponAiming += CallOnWeaponAiming;
        //UIInputManager.OnWeaponButtonReleased += CallOnWeaponReleased;
        //UIInputManager.OnBombAiming += CallOnBombAiming;
        //UIInputManager.OnBombJoysticReleased += CallOnBombReleased;
        //UIInputManager.OnSkillButtonPressed += CallOnSkillUsed;
    }

    private void OnDisable()
    {
        UIInputManager.OnMoving -= CallOnMove;
        UIInputManager.OnMoveJoystickReleased -= CallOnMoveReleased;
        //UIInputManager.OnJumpButtonPressed -= CallOnJump;
        //UIInputManager.OnWeaponAiming -= CallOnWeaponAiming;
        //UIInputManager.OnWeaponButtonReleased -= CallOnWeaponReleased;
        //UIInputManager.OnBombAiming -= CallOnBombAiming;
        //UIInputManager.OnBombJoysticReleased -= CallOnBombReleased;
        //UIInputManager.OnSkillButtonPressed -= CallOnSkillUsed;
    }

    public override void SetInputs(Controller controller)
    {
        throw new System.NotImplementedException();
    }

    public override void RemoveInputs(Controller controller)
    {
        throw new System.NotImplementedException();
    }

    protected override void CallOnMove(Vector2 value)
    {
        OnMoveHold?.Invoke(CameraRelativeDirection.AlignToCamera(value));
    }

    //protected override void CallOnWeaponAiming(Vector2 value)
    //{
    //    OnWeaponAiming?.Invoke(CameraRelativeDirection.AlignToCamera(value));
    //}
}