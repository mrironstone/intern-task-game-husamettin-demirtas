﻿using UnityEngine;
using UnityEngine.Events;

namespace InputSystem
{
    public class MouseAndFingerListener : MonoBehaviour
    {
        [SerializeField] int index;
        [SerializeField] UnityEvent onDownEvent;
        [ReadOnly] [SerializeField] float lastTapTime;
        [Range(.1f,1)] [SerializeField] float doubleTapInterval = .5f;
        [SerializeField] UnityEvent onDoubleTapEvent;
        [SerializeField] UnityEvent onHoldEvent;
        [SerializeField] UnityEvent onUpEvent;

        private void Update()
        {
            if (Input.GetMouseButtonDown(index))
            {
                onDownEvent?.Invoke();
                if(lastTapTime+doubleTapInterval> Time.realtimeSinceStartup)
                {
                    onDoubleTapEvent?.Invoke();
                    lastTapTime = 0;
                }
                else
                    lastTapTime = Time.realtimeSinceStartup;
            }
            else if (Input.GetMouseButton(index))
            {
                onHoldEvent?.Invoke();
            }
            else if (Input.GetMouseButtonUp(index))
            {
                onUpEvent?.Invoke();
            }
        }
    }
}