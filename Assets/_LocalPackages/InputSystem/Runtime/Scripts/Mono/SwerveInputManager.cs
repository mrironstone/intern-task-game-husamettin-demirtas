using UnityEngine;
using System;
using GameLevelSystem;

public class SwerveInputManager : MonoBehaviourSingleton<SwerveInputManager>
{
    public static Action OnTouchReleased;
    float lastFrameFingerPosition;
    float moveFactorX;
    public float MoveFactorX => moveFactorX;
    float referenceScreenWidth = 540;
    [ReadOnly]
    public float HalfLaneSize = 3;

    void OnEnable()
    {
        GameStateManager.OnGameLevelLoaded += SetLaneSize;
    }
    void OnDisable()
    {
        GameStateManager.OnGameLevelLoaded -= SetLaneSize;
    }

    private void Update() {

        if(Input.GetMouseButtonDown(0))
        {
            lastFrameFingerPosition = Input.mousePosition.x;
        }
        else if(Input.GetMouseButton(0))
        {

            moveFactorX = (Input.mousePosition.x - lastFrameFingerPosition) / Screen.width * referenceScreenWidth;
            //moveFactorX = (Input.mousePosition.x - lastFrameFingerPosition);
            if(Mathf.Approximately(moveFactorX, 0))
                moveFactorX = 0;
            //Debug.Log("Move factor x: " + moveFactorX);
            lastFrameFingerPosition = Input.mousePosition.x;
        }
        else if(Input.GetMouseButtonUp(0))
        {
            moveFactorX = 0;
            OnTouchReleased?.Invoke();
        }
    }

    void SetLaneSize()
    {
        HalfLaneSize = ((RunnerLevelData)LevelManager.Instance.LevelData).HalfLaneSize;
    }
}
