﻿using UnityEngine;
using UnityEngine.UI;

namespace ProgressSystem
{
    public abstract class ProgressImage : ProgressEventListener
    {
        [SerializeField] protected Image image;
        [SerializeField] protected bool setOnStart = true;

        private void Start()
        {
            if (setOnStart)
                SetFillAmount(progressType.CurrentProgressRatio);
        }


        public void SetFillAmount(float percentage)
        {
            if (image.IsNull()) return;
            image.fillAmount = percentage;
        }
    }
}