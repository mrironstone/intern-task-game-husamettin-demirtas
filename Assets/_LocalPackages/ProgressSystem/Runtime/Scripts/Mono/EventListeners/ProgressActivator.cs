﻿using UnityEngine;
using UnityEngine.Events;

namespace ProgressSystem
{

    public class ProgressActivator : ProgressEventListener
    {
        [SerializeField] GameObject[] progressObjects;

        protected override void OnLevelUpdated(int level)
        {
            var length = progressObjects.Length;
            var index = Mathf.Clamp(level - 1, 0, length - 1);
            for (int i = 0; i < length; i++)
            {
                progressObjects[i].SetActive(index == i);
            }
        }
    }
}