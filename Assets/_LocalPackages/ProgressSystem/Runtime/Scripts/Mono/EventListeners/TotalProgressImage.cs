﻿namespace ProgressSystem
{
    public class TotalProgressImage : ProgressImage
    {

        protected override void OnProgressRatioChanged(float ratio)
        {
            SetFillAmount(ratio);
        }
    }
}