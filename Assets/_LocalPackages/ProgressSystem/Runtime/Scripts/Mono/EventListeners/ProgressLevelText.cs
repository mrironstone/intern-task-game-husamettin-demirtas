﻿using TMPro;
using UnityEngine;

namespace ProgressSystem
{

    public class ProgressLevelText : ProgressEventListener
    {
        [SerializeField] TMP_Text headerText;
        [SerializeField] string prefix;
        [SerializeField] string suffix;
        [SerializeField] string[] levelNames;

        protected override void OnLevelUpdated(int level)
        {
            SetHeader(level);
        }

        public void SetHeader(int level)
        {
            if (headerText == null) return;
            if (levelNames.Length == 0) return;
            int index = Mathf.Clamp(level - 1, 0, levelNames.Length - 1);
            headerText.SetText(prefix + levelNames[index] + suffix);
        }
    }
}