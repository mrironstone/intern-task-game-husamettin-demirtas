﻿using System;
using UnityEngine;


namespace ProgressSystem
{
    [Serializable]
    public class ProgressTypeDebuggerData
    {
        public ProgressType ProgressType;
        public bool HideUI;
        [Min(10)] public int FontSize = 30;
    }

}