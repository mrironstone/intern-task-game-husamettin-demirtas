﻿using UnityEngine;
using UnityEngine.Events;

namespace ProgressSystem
{
    public class Progressable : MonoBehaviour, IProgressable
    {
        [SerializeField] ProgressType progressType;
        public ProgressType ProgressType => progressType;

        [SerializeField] UnityEvent<int> onProgressModified;
        public UnityEvent<int> OnProgressModified => onProgressModified;


        public void Modify(int value)
        {
            if (value > 0)
                progressType.AddExp(value);
            else
                progressType.RemoveExp(-value);
            OnProgressModified?.Invoke(value);
        }
    }
}