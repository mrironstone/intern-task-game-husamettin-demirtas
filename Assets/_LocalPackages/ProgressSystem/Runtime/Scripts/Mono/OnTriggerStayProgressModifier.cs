﻿using UnityEngine;

namespace ProgressSystem
{
    public class OnTriggerStayProgressModifier : OnTriggerProgressModifier
    {
        float cooldownTime = 1;
        Cooldown cooldown;

        private void Awake()
        {
            cooldown = new Cooldown(cooldownTime);
        }

        private void OnTriggerStay(Collider other)
        {
            if (cooldown.IsCooldownFinished)
            {
                OnTrigger(other);
            }
        }

        protected override void OnTrigger(Collider other)
        {
            var progressable = other.GetComponent<IProgressable>();
            if (IsModifyable(progressable))
            {
                Modify(progressable);
                cooldown.UpdateCooldown();
            }
        }
    }

}