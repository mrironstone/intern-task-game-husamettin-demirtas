﻿using UnityEngine;
using UnityEngine.Events;

namespace ProgressSystem
{
    public interface ICanModifyProgress
    {
        ProgressType ProgressType { get; }
        UnityEvent OnModifyProgress { get; }
        int ModifyValue { get; }

        void Modify(IProgressable progressable);
    }
}