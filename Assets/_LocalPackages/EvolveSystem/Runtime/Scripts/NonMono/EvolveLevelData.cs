﻿using System;
using UnityEngine;

namespace EvolveSystem
{
    [Serializable]
    public class EvolveLevelData
    {
        public GameObject[] EvolveObjects;
        public Animator Animator;

        public void ToggleObjects(bool value)
        {
            foreach (var evolveObject in EvolveObjects)
            {
                evolveObject.SetActive(value);
            }
        }
    }
}