#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
[InitializeOnLoadAttribute]
#endif

public static class LevelLoader
{
#if UNITY_EDITOR

    static LevelLoader()
    {
        EditorApplication.playModeStateChanged += ResetCurrentScene;
    }

    private static void ResetCurrentScene(PlayModeStateChange state)
    {
        if(state==PlayModeStateChange.EnteredPlayMode)
            EditorSceneManager.playModeStartScene = null;
    }

    public static void StartPlayMode()
    {
         EditorApplication.isPlaying = true;
    }

    static void StartGameScene()
    {
        if (EditorApplication.isPlaying)
            EditorApplication.isPlaying = false;
        else
        {

        SetPlayModeStartScene("Assets/Scenes/GameScene.unity");
        StartPlayMode();
        }
    }

    static void StartLevel(int level)
    {
        PlayerPrefs.SetInt("CurrentLevel",level);
        StartGameScene();
    }

    [MenuItem("LevelLoader/StartLevel1")]
    static void StartLevel1()
    {
        StartLevel(0);
    }

    [MenuItem("LevelLoader/StartLevel2")]
    static void StartLevel2()
    {
        StartLevel(1);
    }


    [MenuItem("LevelLoader/StartLevel3")]
    static void StartLevel3()
    {
        StartLevel(2);
    }

    [MenuItem("LevelLoader/StartLevel4")]
    static void StartLevel4()
    {
        StartLevel(3);
    }

    [MenuItem("LevelLoader/StartLevel5")]
    static void StartLevel5()
    {
        StartLevel(4);
    }

    [MenuItem("LevelLoader/StartLevel6")]
    static void StartLevel6()
    {
        StartLevel(5);
    }




    static void SetPlayModeStartScene(string scenePath)
    {
        SceneAsset myWantedStartScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(scenePath);
        if (myWantedStartScene != null)
        {
            Debug.Log("Found Scene " + scenePath);
            EditorSceneManager.playModeStartScene = myWantedStartScene;
        }
        else
            Debug.Log("Could not find Scene " + scenePath);
    }
#endif

}

