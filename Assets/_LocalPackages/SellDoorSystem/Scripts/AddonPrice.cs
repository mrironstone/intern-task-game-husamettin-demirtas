using System;
using System.Collections;
using System.Collections.Generic;
using AddonSystem;
using UnityEngine;

[Serializable]
public class AddonPrice
{
    public AddonObject Addon;
    public int Price;
}
