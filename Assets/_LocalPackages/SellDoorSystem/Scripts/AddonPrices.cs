using AddonSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "AddonPrices", menuName = "AddonSystem/Addon Price Data")]
public class AddonPrices : ScriptableObject
{
    [SerializeField] List<AddonPrice> addonPrices;

    public int GetPrice(AddonObject addon)
    {
        var addonPrice = addonPrices.Find(ap => ap.Addon == addon);
        if (addonPrice!= null)
            return addonPrice.Price;
        else
            return 0;
    }

}
