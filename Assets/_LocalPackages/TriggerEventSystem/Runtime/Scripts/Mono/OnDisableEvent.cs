﻿using UnityEngine;
using UnityEngine.Events;

namespace EventsSystem
{
    public class OnDisableEvent : MonoBehaviour
    {
        [SerializeField] UnityEvent onDisable;

        private void OnDisable()
        {
            onDisable?.Invoke();
        }
    }
}