﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace EventsSystem
{
    public class OnLoadEvent : SceneEvents
    {
        [SerializeField] UnityEvent onLoad;

        private void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        private void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            var name = scene.name;
            Debug.Log(name + " scene is Loaded");
            if (IsConditionRealised(name))
                onLoad?.Invoke();
        }
    }

}