﻿using ScriptableSystem;
using UnityEngine;
using UnityEngine.Events;

namespace EventsSystem
{
    public class OnTimeEvent : MonoBehaviour
    {

        [SerializeField] FloatReference time;
        [SerializeField] UnityEvent onTimeEnded;
        [SerializeField] bool onStart = true;

        private void Start()
        {
            if (onStart == true)
                CallEventWithTimer();
        }

        public void CallEventWithTimer()
        {
            CallEventWithTimer(time.Value);
        }

        public void CallEventWithTimer(float time)
        {
            Invoke(nameof(CallTimeEvent), time);
        }

        void CallTimeEvent()
        {
            onTimeEnded?.Invoke();
        }

    }

}