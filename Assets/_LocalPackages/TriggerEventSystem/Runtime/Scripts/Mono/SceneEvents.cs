﻿using ScriptableSystem;
using UnityEngine;

namespace EventsSystem
{
    public abstract class SceneEvents : MonoBehaviour
    {
        [SerializeField] StringReference[] IncludedScenes;
        [SerializeField] StringReference[] ExcludedScenes;

        protected bool IsConditionRealised(string sceneName)
        {
            bool isConditionRealised = IncludedScenes.Length==0;
            
            for (int i = 0; i < IncludedScenes.Length; i++)
            {
                if (IncludedScenes[i].IsNullOrEmpty)
                {
                    isConditionRealised = true;
                    break;
                }
                else if (sceneName == IncludedScenes[i].Value)
                {
                    isConditionRealised = true;
                    break;
                }
            }

            for (int i = 0; i < ExcludedScenes.Length; i++)
            {
                if (ExcludedScenes[i].IsNullOrEmpty)
                {
                    continue;
                }
                else if (sceneName == ExcludedScenes[i].Value)
                {
                    isConditionRealised = false;
                    break;
                }
            }

            return isConditionRealised;
        }

    }

}