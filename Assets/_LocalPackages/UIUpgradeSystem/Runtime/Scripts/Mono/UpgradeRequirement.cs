﻿using System;
using UnityEngine;

namespace UIUpgradeSystem
{
    public abstract class UpgradeRequirement : ScriptableObject
    {
        public abstract int CurrentLevel { get; set; }

        public abstract bool IsRequirementSatisfied(int level);

        public abstract void OnUpgrade(int level);

        public Action OnUpgraded;

    }
}