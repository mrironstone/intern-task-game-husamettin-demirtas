using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

namespace ToggleSystem
{

    public class ToggleManager : MonoBehaviourSingleton<ToggleManager>
    {

        [SerializeField] GameObject[] cameras;
        [SerializeField] GameObject initialCam;
        [SerializeField] GameObject winCam;
        [SerializeField] bool setOnStart;
        [SerializeField] int index = 0;
        [SerializeField] float targetBlendTime = 0.2f;
        bool isBlendInitialized = false;

        private void OnEnable() {
            GameStateManager.OnGameLevelLoaded += SetInitialCam;
        }
        private void OnDisable() {
            GameStateManager.OnGameLevelLoaded -= SetInitialCam;
        }

        private void Start()
        {
            if (setOnStart)
                SetCurrentToggle();
            SetInitialCam();
        }

        [ContextMenu("Set Current Toggle")]
        public void SetCurrentToggle()
        {
            SetToggle(index);
        }

        public void SetToggle(int value)
        {
            if(!isBlendInitialized)
            {
                Invoke(nameof(InitializeBlend), 1.5f);
                isBlendInitialized = true;
            }

            if (value < 0) return;
            var count = cameras.Length;
            if (count == 0) return;
            var index = Mathf.Min(value, count - 2);
            for (int i = 0; i < count; i++)
            {
                cameras[i].SetActive(i == index);
            }
        }

        public void SetLastToggle(bool value)
        {
            if(!value)
            {
                Debug.LogError("Deactivate last toggle not implemented");
                return;
            }

            var count = cameras.Length;
            if (count == 0) return;
            for (int i = 0; i < count - 1; i++)
            {
                cameras[i].SetActive(false);
            }

            cameras[cameras.Length - 1].SetActive(value);
        }

        public void SetWinToggle(bool value)
        {
            DisableAllCams();
            winCam.SetActive(value);
        }

        public void SetInitialCam()
        {
            var cinemachineBrain = FindObjectOfType<CinemachineBrain>();
            cinemachineBrain.m_DefaultBlend.m_Time = 1.5f;
            DisableAllCams();
            isBlendInitialized = false;
            initialCam.SetActive(true);
        }

        void DisableAllCams()
        {
            var count = cameras.Length;
            if (count == 0) return;
            for (int i = 0; i < count; i++)
            {
                cameras[i].SetActive(false);
            }
        }

        void InitializeBlend()
        {
            var cinemachineBrain = FindObjectOfType<CinemachineBrain>();
            cinemachineBrain.m_DefaultBlend.m_Time = targetBlendTime;
            isBlendInitialized = true;
        }
    }
}