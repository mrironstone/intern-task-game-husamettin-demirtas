using UnityEngine;
using UnityEngine.Events;

namespace HarvestSystem
{
    public class Harvestable : MonoBehaviour
    {
        [SerializeField] MeshRenderer[] notHarvestableParts;
        [SerializeField] protected HarvestPart[] orijinalHarvestParts;
        [SerializeField] float respawnTime = 5;
        HarvestPart[] harvestParts;
        [SerializeField] int damageToHarvestAll;
        int currentDamage;
        Collider harvestableCollider;
        [SerializeField] UnityEvent onResetHarvatable;

        private void Awake()
        {
            harvestableCollider = GetComponent<Collider>();
            SetHarvestable();
        }

        void SetHarvestable()
        {
            var count = orijinalHarvestParts.Length;
            harvestParts = new HarvestPart[count];
            ToogleNotHarvestables(true);
            currentDamage = 0;
            harvestableCollider.enabled = true;
            for (int i = 0; i < count; i++)
            {
                var orijinal = orijinalHarvestParts[i];
                harvestParts[i] = Instantiate(orijinal, orijinal.transform.position, orijinal.transform.rotation, orijinal.transform.parent);
                orijinal.gameObject.SetActive(false);
                harvestParts[i].gameObject.SetActive(true);
            }
        }

        protected void Damage(int value)
        {
            currentDamage += value;
            SetPartsOnDamage();
            if (currentDamage >= damageToHarvestAll)
            {
                harvestableCollider.enabled = false;
                ToogleNotHarvestables(false);
                Invoke(nameof(ResetHarvestable), respawnTime);
            }
        }

        void ToogleNotHarvestables(bool value)
        {
            foreach (var part in notHarvestableParts)
            {
                //part.enabled = value;
                part.gameObject.SetActive(value);
            }
        }

        void SetPartsOnDamage()
        {
            foreach (var part in harvestParts)
            {
                if (part.IsHarvested)
                    continue;
                else if (currentDamage >= part.DamageToHarvest)
                    part.Harvest();
            }
        }

        void ResetHarvestable()
        {
            SetHarvestable();
            onResetHarvatable?.Invoke();
        }
    }
}
