using System;
using System.Collections;
using System.Collections.Generic;
using ScriptableSystem;
using TMPro;
using UnityEngine;

public class ScoreMultiplier : MonoBehaviour
{
    public static Action<int> OnMultiplierHit;
    [SerializeField] int multiplier;
    [SerializeField] TMP_Text multiplierText;
    public int Multiplier { get{return multiplier;}}
    [SerializeField] StringReference stringReference;

    private void Awake() {
        if(multiplierText)
            multiplierText.SetText("x" + multiplier.ToString());
    }

    private void OnTriggerEnter(Collider other) {
        if(!other.gameObject.CompareTag(stringReference.Value)) return;

        OnMultiplierHit?.Invoke(Multiplier);
    }
}
