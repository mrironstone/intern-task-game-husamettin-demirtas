using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace BonusSystem
{
    public class BonusController : MonoBehaviour
    {
        public static Action OnBonusCompleted;
        public virtual Transform PlayerTransform{ get { throw new System.NotImplementedException(); } }
        [SerializeField] protected Transform MoveTransform;
        [SerializeField] protected float MoveDuration = 3f;
        [SerializeField] [ReadOnly] protected int CurrentMultiplier;

        protected virtual void OnEnable() 
        {
            ScoreMultiplier.OnMultiplierHit += OnMultiplierHit;
        }

        protected virtual void OnDisable() 
        {
            ScoreMultiplier.OnMultiplierHit -= OnMultiplierHit;
        }

        public virtual void StartBonus()
        {
            Debug.Log("Bonus started");
            StartMove();
        }

        protected virtual void StartMove()
        {
            PlayerTransform.DOMove(MoveTransform.position, MoveDuration);
        }

        protected virtual void OnMultiplierHit(int value)
        {
            if(value > CurrentMultiplier)
                CurrentMultiplier = value;
        }
    }
}