using UnityEngine;

public class SkyboxColorSetter : MonoBehaviour
{
    [SerializeField] Material skyBoxMat;

    void Start()
    {
        if (skyBoxMat)
        {
            RenderSettings.skybox = skyBoxMat;
        }
    }
}
