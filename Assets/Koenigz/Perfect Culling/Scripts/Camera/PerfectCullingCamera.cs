﻿// Perfect Culling (C) 2021 Patrick König
//

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Rendering;

namespace Koenigz.PerfectCulling
{
    [RequireComponent(typeof(Camera))]
    public class PerfectCullingCamera : MonoBehaviour
    {
        public static List<PerfectCullingCamera> AllCameras = new List<PerfectCullingCamera>();
        
        public bool ShowInGameStats { get; set; }
        
        public bool VisualizeFrustumCulling { get; set; } = true;
        
        public int LastTotalVertices { get; private set; }
        public int LastVisibleVertices  { get; private set; }
        public int LastCulledVertices => LastTotalVertices - LastVisibleVertices;
        
        public int LastVisible { get; private set; }
        public int LastTotal { get;private set; }
        public int LastCulled => LastTotal - LastVisible;

        [Tooltip("Allows to take into account neighbor cells to prevent popping issues. It's a great way to compensate for a too sparse bake. This comes with a minor performance impact.\n\n" +
                 "You can achieve even better results without performance implications by baking this in by using the Merge-Downsample feature for your bakes.")]
        [Range(0, 2)]
        public int NeighborCellIncludeRadius = 0;

        public PerfectCullingVisibilityLayer visibilityLayer = PerfectCullingVisibilityLayer.All;

        public int LastFrameHash => m_lastFrameHash;

        private bool m_invertCulling = false;

        public bool InvertCulling
        {
            get => m_invertCulling;
            set
            {
                m_invertCulling = value;
                
                SetDirty();
            }
        }

        // We use this as a more efficient HashSet
        private static readonly bool[] m_visibleRenderers = new bool[PerfectCullingConstants.MaxRenderers];

        private static int m_lastFrame = -1;

        private static int m_lastFrameHash = -1;

        private Camera m_camera;

        private static Vector3[] m_offsets = new Vector3[]
        {
            // 0, 0, 0 needs to be first because IncludeNeighborCells depends on that
            new Vector3(0, 0, 0),
            
            new Vector3(1, 0, 0),
            new Vector3(-1, 0, 0),
            
            new Vector3(0, 0, 1),
            new Vector3(0, 0, -1),

            new Vector3(0, 1, 0),
            new Vector3(0, -1, 0),
        };

        void Awake()
        {
            m_camera = GetComponent<Camera>();
        }
        
        private void OnEnable()
        {
            SetDirty();
            
            AllCameras.Add(this);

            // https://issuetracker.unity3d.com/issues/hdrp-renderpipelinemanager-dot-currentpipeline-is-null-for-the-first-few-frames-of-playmode
            // Concluding that we CANNOT check for if (RenderPipelineManager.currentPipeline != null) to detect SRP here
            
#if UNITY_2019_1_OR_NEWER
            RenderPipelineManager.beginCameraRendering += OnBeginCameraRendering;
#endif
            Camera.onPreCull += CamPreCull;
        }

        private void OnDisable()
        {
            // Lets make sure we do this first to not get into a situation where we didn't unsubscribe
#if UNITY_2019_1_OR_NEWER
            RenderPipelineManager.beginCameraRendering -= OnBeginCameraRendering;
#endif
            Camera.onPreCull -= CamPreCull;
            
            SetDirty();
            
            // Execute this before ToggleAllRenderers - just in case we run into an exception.
            AllCameras.Remove(this);

            // Toggle everything back on. Just in case.
            foreach (var volume in PerfectCullingVolume.AllVolumes)
            {
                // We also force null checks because OnDisable() might have been called as part of an active destruction process (scene change, etc.)
                volume.ToggleAllRenderers(true, true);
            }
            
            LastTotal = 0;
            LastVisible = 0;
            
            System.Array.Clear(m_visibleRenderers, 0, m_visibleRenderers.Length);
        }

        void OnBeginCameraRendering(ScriptableRenderContext context, Camera camera)
        {
            CamPreCull(camera);
        }

        private void DoVisualizeFrustumCulling(Camera camera)
        {
#if UNITY_EDITOR
            if (!VisualizeFrustumCulling)
            {
                return;
            }
            
            if (UnityEditor.Selection.activeGameObject != m_camera.gameObject)
            {
                return;
            }
            
            bool isSceneCamera = false;
            
            foreach (Camera sceneCamera in UnityEditor.SceneView.GetAllSceneCameras())
            {
                if (sceneCamera == camera)
                {
                    isSceneCamera = true;

                    break;
                }
            }

            if (!isSceneCamera)
            {
                return;
            }

#if UNITY_2019_1_OR_NEWER
            void EndCameraRendering(ScriptableRenderContext context, Camera preRenderCamera)
            {
                if (preRenderCamera != camera)
                {
                    return;
                }

                preRenderCamera.ResetCullingMatrix();
                
                RenderPipelineManager.endCameraRendering -= EndCameraRendering;
            }
#endif
            
            void PostRender(Camera preRenderCamera)
            {   
                if (preRenderCamera != camera)
                {
                    return;
                }

                preRenderCamera.ResetCullingMatrix();
                
                Camera.onPostRender -= PostRender;
            }

            camera.cullingMatrix = m_camera.cullingMatrix;

#if UNITY_2019_1_OR_NEWER
            RenderPipelineManager.endCameraRendering += EndCameraRendering;
#endif
            
            Camera.onPostRender += PostRender;
#endif
        }
        
        private void CamPreCull(Camera camera)
        {
            if (camera != m_camera)
            {
                DoVisualizeFrustumCulling(camera);
                
                // Another camera rendering. We are not interested in it.
                return;
            }

            Vector3 camPos = transform.position;

            // We calculate a hash for all visible cell indices to tell whether our camera is dirty or not.
            int thisFrameHash = 13;

            int maxSamples = NeighborCellIncludeRadius != 0 ? m_offsets.Length : 1;
            
            foreach (var volume in PerfectCullingVolume.AllVolumes)
            {
                if (((int)volume.visibilityLayer & (int)visibilityLayer) == 0)
                {
                    // Not assigned to the same layer. We just ignore it.
                    continue;
                }
                
                for (int neighborIndex = 0; neighborIndex < maxSamples; ++neighborIndex)
                {
                    for (int j = 1; j <= NeighborCellIncludeRadius + 1; ++j)
                    {
                        unchecked
                        {
                            int index = volume.GetIndexForWorldPos(camPos + Vector3.Scale(
                                m_offsets[neighborIndex] * j, volume.volumeBakeData.cellSize), out bool isOutOfBounds);

                            if (volume.outOfBoundsBehaviour == PerfectCullingBakingBehaviour.EOutOfBoundsBehaviour.Cull && isOutOfBounds)
                            {
                                continue;
                            }

                            thisFrameHash = thisFrameHash * 17 + index;
                        }
                    }
                }
            }

            // Hashes match. Nothing to do.
            if (m_lastFrameHash == thisFrameHash)
            {
                return;
            }
            
            // Only want to toggle everything off once per frame.
            // This makes sure that we don't disable renderers that another camera enabled before us.
            if (Time.frameCount != m_lastFrame)
            {
                foreach (var volume in PerfectCullingVolume.AllVolumes)
                {
                    volume.ToggleAllRenderers(InvertCulling);
                }
                
                m_lastFrame = Time.frameCount;
            }
            
            LastTotal = 0;
            LastVisible = 0;

            int totalVisible = 0;

            m_lastFrameHash = thisFrameHash;

            LastTotalVertices = 0;
            LastVisibleVertices = 0;
            
            foreach (var volume in PerfectCullingVolume.AllVolumes)
            {
                if (((int)volume.visibilityLayer & (int)visibilityLayer) == 0)
                {
                    // Not assigned to the same layer. We just ignore it.
                    continue;
                }
                
                System.Array.Clear(m_visibleRenderers, 0, m_visibleRenderers.Length);

                LastTotalVertices += volume.TotalVertexCount;
                
                for (int neighborIndex = 0; neighborIndex < maxSamples; ++neighborIndex)
                {
                    for (int j = 1; j <= NeighborCellIncludeRadius + 1; ++j)
                    {
                        int worldPosIndex = volume.GetIndexForWorldPos(camPos + Vector3.Scale(
                            m_offsets[neighborIndex] * j, volume.volumeBakeData.cellSize), out bool isOutOfBounds);

                        if (volume.outOfBoundsBehaviour == PerfectCullingBakingBehaviour.EOutOfBoundsBehaviour.Cull && isOutOfBounds)
                        {
                            continue;
                        }
                        
                        PerfectCullingTemp.ListUshort.Clear();
                        volume.GetIndicesForWorldPos(
                            camPos + Vector3.Scale(m_offsets[neighborIndex] * j,
                                volume.volumeBakeData.cellSize), PerfectCullingTemp.ListUshort);

                        for (int indexIndices = 0; indexIndices < PerfectCullingTemp.ListUshort.Count; ++indexIndices)
                        {
                            int index = PerfectCullingTemp.ListUshort[indexIndices];

                            PerfectCullingBakeGroup r = volume.GetRendererForId(index);

                            if (m_visibleRenderers[index])
                            {
                                // Already processed
                                continue;
                            }
                            
                            r.Toggle(!InvertCulling);

                            m_visibleRenderers[index] = true;

                            LastVisibleVertices += r.vertexCount;
                            ++totalVisible;
                        }
                    }
                }

                LastTotal += volume.RenderersCount;
            }

            LastVisible += totalVisible;
        }

        void SetDirty()
        {
            m_lastFrameHash = -1;
        }
        
#if UNITY_EDITOR
        private int m_guiLastFrameHash = -1;
        private string m_guiLastText = null;
        
        private void OnGUI()
        {
            if (!ShowInGameStats)
            {
                return;
            }
            
            if (m_guiLastFrameHash != LastFrameHash || m_guiLastText == null)
            {
                m_guiLastText = "* Perfect Culling Stats *\n"
                                + $"Total renderers: {LastTotal}\n"

                                + $" - Culled: {LastCulled} ({Mathf.Round((LastCulled / (float) LastTotal) * 100f)}%)\n"
                                + $" - Visible: {LastVisible}\n"
                                + $" - Culled verts: {PerfectCullingUtil.FormatNumber(LastCulledVertices)}/{PerfectCullingUtil.FormatNumber(LastTotalVertices)} ({Mathf.Round((LastCulledVertices / (float) LastTotalVertices) * 100f)}%)\n"
                                + $" - Hash: {LastFrameHash}\n";

                m_guiLastFrameHash = LastFrameHash;
            }

            GUI.skin.box.fontSize = 30;
            GUI.skin.box.alignment = TextAnchor.UpperLeft;

            GUILayout.Box(m_guiLastText, System.Array.Empty<GUILayoutOption>());
        }
#endif
    }
}