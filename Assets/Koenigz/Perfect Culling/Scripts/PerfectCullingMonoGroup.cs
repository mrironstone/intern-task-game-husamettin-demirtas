﻿// Perfect Culling (C) 2021 Patrick König
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Koenigz.PerfectCulling
{
    public abstract class PerfectCullingMonoGroup : MonoBehaviour
    {
        public virtual List<Renderer> Renderers => throw new System.NotImplementedException();
    }
}