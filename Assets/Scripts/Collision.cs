using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision : MonoBehaviour
{
    [SerializeField] private PlayerLevelManager playerLevelManager;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("LevelDownGate"))
        {
            playerLevelManager.LevelDown();
        }
        else if (other.CompareTag("LevelUpGate"))
        {
            playerLevelManager.LevelUp();
        }
        
        else if (other.gameObject.CompareTag("Obstacle"))
        {
            int index = ChainSystem.instance.chain.IndexOf(gameObject);
            ChainSystem.instance.BreakChain(index);
        }
        
        else if (other.gameObject.CompareTag("Warrior"))
        {
            // kod cok hizli aktigi icin bi kontrol yapmak iyi oluyor
            if (!ChainSystem.instance.chain.Contains(other.gameObject))
            {
                other.GetComponent<CapsuleCollider>().isTrigger = false;
                other.gameObject.tag = "Untagged";

                ChainSystem.instance.AddToChain(other.gameObject, ChainSystem.instance.chain.Count - 1);

            }
        }
        
    }

}
