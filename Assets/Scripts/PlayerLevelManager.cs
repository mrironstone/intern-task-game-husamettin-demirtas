using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerLevelManager : MonoBehaviour
{
    private int level;

    [SerializeField] private ParticleSystem transformationEffect;
    
    [SerializeField] private List<GameObject> levels;

    private void Start()
    {
        level = 1;
    }

    public void LevelUp()
    {
        // bir sonraki indexteki yer var mi diye bakiyoruz
        if (level+1 <= levels.Count)
        {
            
            levels[level-1].gameObject.SetActive(false);
            this.transform.DOScale(1.5f, 0.1f).OnComplete(() =>
                this.transform.DOScale(new Vector3(1, 1, 1), 0.1f));
            // transformationEffect.Play();
            levels[level-1 +1].gameObject.SetActive(true);
            level++;
        }
        
    }
    public void LevelDown()
    {
        // bir önceki indexteki yer var mi diye bakiyoruz
        if (level-1-1 >= 0)
        {
            levels[level-1].gameObject.SetActive(false);
            this.transform.DOScale(new Vector3(.2f, .2f, .2f), 0.1f).OnComplete(() =>
                this.transform.DOScale(new Vector3(1, 1, 1), 0.1f));
            // transformationEffect.Play();
            levels[level-1 -1].gameObject.SetActive(true);
            level--;
        }
        
    }

}
