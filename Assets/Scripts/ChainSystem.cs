using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using DG.Tweening;
using UnityEditor.Animations;

public class ChainSystem : MonoBehaviour
{
    // Start is called before the first frame update

    public static ChainSystem instance;
    public float movementDelay = 0.25f;


    public List<GameObject> chain = new List<GameObject>();

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Update()
    {
        MoveListElements();
    }

    public void AddToChain(GameObject other, int index)
    {
        other.transform.parent = transform;
        Vector3 newPos = chain[index].transform.localPosition;
        // aralara bosluk vermek icin
        newPos.z += 1.2f;
        other.transform.localPosition = newPos;
        
        chain.Add(other);
            
        StartCoroutine(MakeWaveEffect());
    }

    private IEnumerator MakeWaveEffect()
    {
        for (int i = chain.Count - 1; i > 0; i--)
        {

            int index = i;
            Vector3 scale = new Vector3(1, 1, 1);
            scale *= 1.5f;
            chain[index].transform.DOScale(scale, 0.1f).OnComplete(() =>
             chain[index].transform.DOScale(new Vector3(1, 1, 1), 0.1f));
            yield return new WaitForSeconds(0.05f);
        }
    }


    public void MoveListElements()
    {
        for (int i = 1; i < chain.Count; i++)
        {
            Vector3 pos = chain[i].transform.localPosition;
            pos.x = chain[i-1].transform.localPosition.x;
            chain[i].transform.DOLocalMove(pos,movementDelay);
        }
    }

    public void MoveOrigin()
    {
        for (int i = 1; i < chain.Count; i++)
        {
            Vector3 pos = chain[i].transform.localPosition;
            pos.x = chain[0].transform.localPosition.x;
            chain[i].transform.DOLocalMove(pos, movementDelay);
        }
    }

    public void BreakChain(int index)
    {
        int startPoint = chain.Count-1;
        for (int i = startPoint; i <= index; i++)
        {
            GameObject dumpedOne = chain[i].gameObject;
            chain.RemoveAt(chain.Count - 1);
            dumpedOne.GetComponent<Rigidbody>().AddExplosionForce
                (1f,dumpedOne.transform.localPosition, 2f,2f);
            dumpedOne.tag = "Warrior";
        }
    }

}
